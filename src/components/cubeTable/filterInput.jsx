import React from 'react';
import Input from "antd/lib/input";
import Icon from "antd/lib/icon";
import {connect} from "react-redux";
import {populateMeta} from "../../store/actions/tableActions";

const FilterInput = ({cubeConst, search, dataIndex, populateMeta}) => {
  const onInputChange = e => {
    populateMeta(cubeConst, {
      search: {
        ...search,
        [dataIndex]: e.target.value
      }
    })
  };
  const emitEmpty = () => {
    populateMeta(cubeConst, {search: {...search, [dataIndex]: ''}})
  };
  return (
    <div className="custom-filter-dropdown">
      <Input
        name={dataIndex}
        suffix={search[dataIndex] && <Icon type="close-circle" onClick={emitEmpty}/>}
        autoFocus
        placeholder="Поиск"
        value={search[dataIndex]}
        onChange={onInputChange}
      />
    </div>
  )
};

function mapState(state, ownProps) {
  const table = state.table[ownProps.cubeConst] || {};
  return {
    search: table.search || {},
  }
}

export default connect(mapState, {populateMeta})(FilterInput);