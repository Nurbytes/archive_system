import React, {Component} from 'react';
import Layout from 'antd/lib/layout';
import Menu from "antd/lib/menu";
import PropTypes from 'prop-types';
import Icon from "antd/lib/icon";
import {Link} from "react-router-dom";
import './sider.scss';

function getCollections(t) {
  return [
    {
      name: t('MANNING'), //Комплектование
      pathStart: '/manning',
      iconType: 'solution',
      priv: 'acquisition',
      children: [
        {
          name: t('FUNDMAKERS'), // Фондообразователи
          path: '/manning/fundmakers',
          priv: 'fundmakers'
        },
        {
          name: t('SOURCE_ACQUISITION'), // Источники комплектования
          path: '/manning/asquisitions',
          priv: 'sourceacquisition'
        },
        {
          name: t('ACQUISITION_WORKS'), // Работы по комплектованию
          path: '/manning/works',
          priv: 'acquisitionwork'
        },
        /*          {
                    name: t('RECEIVING_SCHEDULE'), // График приема архивных документов
                    path: '/sourcing/schedule',
                    priv: 'receivingschedule'
                  },
                  {
                    name: t('INSPECTION_PLAN'), // План проверки источников комплектования
                    path: '/sourcing/checking',
                    priv: 'inspectionplan'
                  },*/
        {
          name: t('ACQUISITION_REPORTS'), // Отчеты по комплектованию
          path: '/manning/reports',
          priv: 'acquisitionreport'

        }
      ]
    },
    {
      name: t('KEEPING'), // Учет и хранение
      iconType: 'check-circle-o',
      pathStart: '/keeping',
      priv: 'storage',
      children: [
        {
          name: t('FUNDS'), // Архивные фонды
          path: '/keeping/funds',
          priv: 'funds'
        },
        {
          name: t('INSPECTION_SCHEDULE'), // График проверки наличия
          path: '/keeping/inspectionSchedule',
          priv: 'inspectionschedule'
        },
        {
          name: t('KEEPING_WORKS'), // Работы по учету и хранению
          path: '/keeping/works',
          priv: 'storagework'
        },
        {
          name: t('KEEPING_REPORTS'), // Отчеты по учету и хранению
          path: '/keeping/reports',
          priv: 'storagereport'
        }
      ]
    },
    {
      name: t('SRA'), // Ведение научно-справочного аппарата
      iconType: 'switcher',
      pathStart: '/sra',
      priv: 'nsa',
      children: [
        {
          name: t('CLASSIFICATION_SCHEM'), // Схемы классификации
          path: '/sra/classificationSchemas',
          priv: 'classificationschem'
        },
        /*
         {
         name: t('Справочники и) классификаторы',
         path: '/nsa/searchPage'
         },
         */
        {
          name: t('FUND_DESCRIPTION'), // Описание архивных фондов
          path: '/sra/fundsDescription',
          priv: 'funddescription'
        },
        {
          name: t('UNIT_DESCRIPTION'), // Описание единиц хранения
          path: '/sra/casesDescription',
          priv: 'unitdescription'
        },
        {
          name: t('DOC_DESCRIPTION'), // Описание архивных документов
          path: '/sra/documentsDescription',
          priv: 'docdescription'
        },
        {
          name: t('NSA_WORK'), // Работы по ведению НСА
          path: '/sra/sraWorks',
          priv: 'nsawork'
        },

        /*{
         name: t('Тематические о)бзоры',
         path: '/createCollection'
         },*/
      ]
    },
    {
      name: t('USING'), // Использование
      iconType: 'desktop',
      pathStart: '/uses',
      priv: 'applying',
      children: [
        {
          name: t('READING_ROOM'), // Читальный зал
          path: '/uses/readingRoom',
          priv: 'readingroom'
        },
        {
          name: t('RESEARCHES'), // Запросы на получение архивной справки
          path: '/uses/inquiryReq',
          priv: 'services'
        },
        {
          name: t('RESEARCHER_CABINET'), // Личный кабинет исследователя
          path: '/uses/researcherCabinet',
          priv: 'researcher-cabinet'
        },
        {
          name: t('APPLYING_WORK'), // Работы по использованию
          path: '/works/archiveServiceWorks',
          priv: 'applyingwork'
        },
        /*
                  {
                    name: t('RESEARCHERS'), // Исследователи
                    path: '/uses/researchers',
                    priv: 'researchers'
                  },
        */
      ]
    },
    {
      name: t('MANAGING'), // Управление
      iconType: 'pie-chart',
      pathStart: '/managing',
      priv: 'control',
      children: [
        {
          name: t('PASSPORT'), // Паспорт архива
          path: '/managing/archivePassport',
          priv: 'passport'
        },
        {
          name: t('CONTROL_REPORT'), // Отчеты
          path: '/managing/reports',
          priv: 'control-report'
        },
        {
          name: t('CONTROL_WORK'), // Работы по управлению архивом
          path: '/works/controlWorks',
          priv: 'controlwork'
        }
      ]
    },
    {
      name: t('ARCHIVE_ADMINISTRATION'), // Администрирование
      iconType: 'key',
      pathStart: '/admin',
      priv: 'archive-administration',
      children: [
        {
          name: t('USERS'), // Пользователи
          path: '/admin/users',
          priv: 'usrs'
        },
        {
          name: t('USER_ROLES'), // Роли пользователей
          path: '/users-roles',
          priv: 'usrsroles'
        }/*,
          {
            name: 'для тестов',
            path: '/test'
          },
            {
              name:'Роли пользователей тест',
                path:'/users-roles'
            }*/
      ]
    }
  ]
};

class Sider extends Component {

  renderCollections = () => {
    return getCollections(this.props.t)
    .filter(nav => !nav.priv || this.props.privs.includes(nav.priv))
      .map((nav, idx) => {
        return (
          !nav.children ?
            <Menu.Item key={idx}>
              <Link to={nav.path} className="sider-nav">
                <Icon type={nav.iconType} style={{fontSize: '16px'}}/>
                {nav.name}
              </Link>
            </Menu.Item> :
            <Menu.SubMenu
              key={idx}
              title={<span><Icon type={nav.iconType}/><span>{nav.name}</span></span>}
            >
              {nav.children
                .filter(child => !child.priv || this.props.privs.includes(child.priv))
                .map(child => {
                  return (
                    <Menu.Item key={child.name}>
                      <Link to={child.path} className="sider-nav">
                        {child.name}
                      </Link>
                    </Menu.Item>
                  )
                })}
            </Menu.SubMenu>
        );
      });
  };

  render() {
    return (
      <Layout.Sider
        collapsible
        defaultCollapsed
        collapsedWidth={50}
        theme='light'
        width={250}
        style={{ overflowY: 'auto', overflowX: 'hidden' }}
      >
        <Menu mode="inline">
          {this.renderCollections()}
        </Menu>
      </Layout.Sider>
    );
  }
}

Sider.propTypes = {
  privs: PropTypes.arrayOf(
    PropTypes.string
  ).isRequired
};

export default Sider;