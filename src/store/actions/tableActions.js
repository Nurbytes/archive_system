export const DESTROY_TABLE = 'DESTROY_TABLE';
export const POPULATE_META = 'POPULATE_META';

export const destroyTable = cubeConst => dispatch => {
  dispatch({
    type: DESTROY_TABLE,
    cubeConst
  })
};

export const populateMeta = (cubeConst, meta) => dispatch => {
  dispatch({
    type: POPULATE_META,
    cubeConst,
    meta
  })
};