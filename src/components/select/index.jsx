import React from "react";
import { List } from "react-virtualized";
import Select, { createFilter, components } from "react-select";
import CreatableSelectComp from 'react-select/lib/Creatable';
import './select.scss';

const stringify = option => option.label;
const customFilter = createFilter({
  ignoreCase: true,
  trim: false,
  stringify
});

const MultiValueLabel = props => {
  const { children } = props;
  return (
    <div title={children} children={children.slice(0, 5) + '...'} />
  );
};

const SingleValue = ({ children, ...props }) => (
  <components.SingleValue {...props}>
    <span title={children}>{children}</span>
  </components.SingleValue>
);

export default (props) => <Select
  components={{ MultiValueLabel, SingleValue }}
  className={'react-select-container ' + (props.selectClassName || '')}
  classNamePrefix='react-select'
  isClearable
  isDisabled={!!props.disabled}
  {...props}
  filterOption={customFilter}
/>

export const CreatableSelect = props => <CreatableSelectComp
  components={{ MultiValueLabel }}
  className={'react-select-container ' + (props.selectClassName || '')}
  classNamePrefix='react-select'
  isClearable
  isDisabled={!!props.disabled}
  {...props}
/>;

const MenuList = props => {
  return (
    <List
      width={300}
      height={props.options.length > 1 ? 300 : 40}
      rowCount={props.options.length}
      rowHeight={40}
      rowRenderer={({ key, index, style }) => {
        return (
          <div title={props.children[index] && props.children[index].props.children} key={key} style={{...style, whiteSpace: 'nowrap'}}>
            {props.children[index]}
          </div>
        );
      }}
    />
  );
};

export const SelectVirt = (props) => <Select
  isClearable
  filterOption={customFilter}
  components={{ MenuList, MultiValueLabel, SingleValue }}
  className={'react-select-container long-selected-menu ' + (props.selectClassName || '')}
  classNamePrefix='react-select'
  isDisabled={!!props.disabled}
  {...props}
/>

