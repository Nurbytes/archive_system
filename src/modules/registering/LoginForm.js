import React from 'react';
import {Field, ErrorMessage} from 'formik';
import Form from 'antd/lib/form';
import Button from "antd/lib/button";
import {renderInput} from "../../utils/form_components";
import {Link} from "react-router-dom";

const FormItem = Form.Item;

const LoginForm = ({status, touched, isSubmitting, handleSubmit, handleReset, suffix, showPassword}) => {
  return (
    <Form onReset={handleReset} onSubmit={handleSubmit}>
      <Field autoFocus type="text" name="login" placeholder='Login' component={renderInput}/>
      <ErrorMessage name="login" component="div"/>
      <Field type={showPassword ? "text" : "password"} name="password" suffix={suffix} placeholder='Password' component={renderInput}/>
      <ErrorMessage name="password" className="error" component="div"/>
      <FormItem>
        <Link className="login-form__link" to="/forgot_password">Забыли пароль?</Link>
      </FormItem>
      <FormItem>
        {status && status.submitFailed && <div style={{color: 'red'}}>{String(status.msg)}</div>}
        <Button htmlType='submit' type="primary" disabled={isSubmitting}>
          Submit
        </Button>
      </FormItem>
      <FormItem>
        <Link to="/signup">Регистрация</Link>
      </FormItem>
    </Form>
  );
};

export default LoginForm;