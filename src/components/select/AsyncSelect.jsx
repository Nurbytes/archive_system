import React, {Component} from 'react';
import {connect} from "react-redux";
import PropTypes from 'prop-types';
import ReactSelect, { SelectVirt } from './index';

class AsyncSelect extends Component {

  state = {
    options: [],
    isLoading: false
  };
  getOptions = async () => {
    try {
      if (!this.props.options) {
        this.setState({isLoading: true});
        const res = await this.props.getOptions();
        this.setState({isLoading: false});
        if (res.success) {
          this.setState({options: res.data});
        }
      }
    } catch (err) {
      console.error(err);
      this.setState({isLoading: false});
    }
  };

  componentDidMount() {
    this.props.options && this.setState({options: this.props.options});
  }
  componentDidUpdate(prevProps) {
    if(this.props.options !== prevProps.options) {
      this.setState({ options: this.props.options });
    }
  }

  render() {
    const {lng, getOptions, options, virtualized, ...rest} = this.props;
    const Select = virtualized ? SelectVirt : ReactSelect;
    return (
      <Select
        options={(this.state.options || [])
          .map(opt => ({value: opt.id, label: opt.name[lng]}))
        }
        onMenuOpen={this.getOptions}
        isLoading={this.state.isLoading}
        {...rest}
      />
    );
  }
}

AsyncSelect.propTypes = {
  getOptions: PropTypes.func.isRequired,
  forceFetch: PropTypes.bool
};

function mapState(state, ownProps) {
  const options = state.select[ownProps.customKey || ownProps.name];
  return {
    lng: state.lng,
    options,
  }
}

export default connect(mapState)(AsyncSelect);