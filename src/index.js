import React from 'react';
import ReactDOM from 'react-dom';
import 'moment/locale/ru';
import 'moment/locale/kk';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { I18nextProvider } from 'react-i18next';
import i18n from './utils/i18n.js';
import moment from "moment";
moment.locale('ru');

ReactDOM.render(<I18nextProvider i18n={i18n}>
  <App />
</I18nextProvider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.register();
