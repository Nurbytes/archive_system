import React from 'react';
import Form from 'antd/lib/form';
import Input from 'antd/lib/input';
import {ErrorMessage} from "formik";
import Select  from '../components/select';
import AsyncSelect from '../components/select/AsyncSelect';
import DPickerTOFI from "../components/DPickerTOFI";
import UploadButton from "../components/uploadButton";
import InputLng from "../components/InputLng";

const FormItem = Form.Item;

export const renderInput = ({ field, form: {errors, touched, setFieldValue}, children, formItemLayout, formItemClass, normalize,  ...rest }) => {
  const handleChange = e => {
    if(normalize) return setFieldValue(field.name, normalize(e.target.value));
    field.onChange(e);
  };
  return <FormItem
    label={rest.label}
    validateStatus={(errors[field.name] && touched[field.name]) ? 'error': ''}
    className={formItemClass}
    {...formItemLayout}
  >
    {children ||
    <Input
      {...field}
      {...rest}
      onChange={handleChange}
    />
    }
    <ErrorMessage style={{ color: 'red' }} name={field.name} component="div"/>
  </FormItem>
};
export const renderInputLang = ({ field, form: {errors, touched, setFieldValue}, children, formItemLayout, formItemClass, ...rest }) => {
  let inpRef = React.createRef();
  return <FormItem
    label={rest.label}
    validateStatus={(errors[field.name] && touched[field.name]) ? 'error': ''}
    className={`with-lang ${formItemClass}`}
    {...formItemLayout}
  >
    {children ||
    <>
      <InputLng
        {...field}
        {...rest}
        setFieldValue={setFieldValue}
        ref={inp => inpRef = inp}
      />
    </>
    }
    <ErrorMessage style={{ color: 'red' }} name={field.name} component="div"/>
  </FormItem>
};
export const renderSelect = ({ field, form: {errors, touched, setFieldValue}, children, formItemLayout, formItemClass, ...rest }) => {
  const opt = (rest.options || []).find(o => o.value === field.value);
  const selectValue = typeof field.value !== 'object'
    ? opt
    : field.value;
  return <FormItem
    label={rest.label}
    validateStatus={(errors[field.name] && touched[field.name]) ? 'error': ''}
    className={formItemClass}
    {...formItemLayout}
  >
    {children ||
    <Select
      {...field}
      {...rest}
      value={selectValue}
      onChange={s => {
        rest.onSelectChange && rest.onSelectChange(field.name, s ? s.value : '');
        setFieldValue(field.name, s);
      }}
    />
    }
    <ErrorMessage style={{ color: 'red' }} name={field.name} component="div"/>
  </FormItem>
};
export const renderAsyncSelect = ({ field, form: {errors, touched, setFieldValue}, children, formItemLayout, formItemClass, ...rest }) => {
  return <FormItem
    label={rest.label}
    validateStatus={(errors[field.name] && touched[field.name]) ? 'error': ''}
    className={formItemClass}
    {...formItemLayout}
  >
    {children ||
    <AsyncSelect
      {...field}
      {...rest}
      onChange={s => {
        rest.onSelectChange && rest.onSelectChange(field.name, s ? s.value : '');
        setFieldValue(field.name, s);
      }}
    />
    }
    <ErrorMessage style={{ color: 'red' }} name={field.name} component="div"/>
  </FormItem>
};
export const renderDPicker = ({ field, form: {errors, touched, setFieldValue}, children, formItemLayout, formItemClass, ...rest }) => {
  return <FormItem
    label={rest.label}
    validateStatus={(errors[field.name] && touched[field.name]) ? 'error': ''}
    className={formItemClass}
    {...formItemLayout}
  >
    {children ||
    <DPickerTOFI
      format="DD-MM-YYYY"
      {...field}
      {...rest}
      onChange={s => setFieldValue(field.name, s)}
    />
    }
    <ErrorMessage style={{ color: 'red' }} name={field.name} component="div"/>
  </FormItem>
};
export const renderUploadButton = ({ field, form: {errors, touched, setFieldValue}, children, formItemLayout, formItemClass, ...rest }) => {
  return <FormItem
    label={rest.label}
    validateStatus={(errors[field.name] && touched[field.name]) ? 'error': ''}
    className={formItemClass}
    {...formItemLayout}
  >
    {children ||
    <UploadButton
      format="DD-MM-YYYY"
      {...field}
      {...rest}
      value={field.value || []}
      onChange={s => setFieldValue(field.name, s)}
    />
    }
    <ErrorMessage style={{ color: 'red' }} name={field.name} component="div"/>
  </FormItem>
};
