import React from 'react';
import {Link} from "react-router-dom";
import groupBy from 'lodash/groupBy';
import uniq from 'lodash/uniq';
import ShowComponent from "../../hoc/ShowComponent";
import Menu from "antd/lib/menu";
import Icon from "antd/lib/icon";
import Dropdown from "antd/lib/dropdown";
import Badge from "antd/lib/badge";
// import message from 'antd/lib/message';

const LoggedInNav = props => {
  const {
    langMenu, basket, basketState, basketIsShown, showBasket, hideBasket, router, clearBasket,
    t, removeCaseFromBasket, addCaseToBasket, changeCaseInBasket, user, tofiConstants
  } = props;

  const handleListItemClick = item => {
    if (basket.some(el => el.key === item.key)) removeCaseFromBasket(item);
    else addCaseToBasket(item);
  };

  const recieveOrder = () => {
    if (basket.length === 0) return;
    let isOuterUser = ['clsResearchers', 'clsTempUsers']
      .some(c => tofiConstants[c].id == user.cls);
    const userId = user.obj;
    const archiveId = basket[0].archiveId;
    const datas = [];
    const groupByFund = groupBy(basket, item => item.fundId);
    for (let key in groupByFund) {
      const fund = {fundId: key, invs: []};
      const groupByInv = groupBy(groupByFund[key], item => item.inventoryId);
      for (let key in groupByInv) {
        const item = groupByInv[key];
        const inv = {invId: key, cases: []};
        const groupByCase = groupBy(item, item => item.caseId);
        for (let key in groupByCase) {
          const item = groupByCase[key];
          const docs = [];
          const caseThemes = item.map(o => {
            o.docId && docs.push(o.docId);
            if (o.propStudy.type === 'theme') isOuterUser = true;
            return String(o.propStudy.value);
          });
          const caseWorksKey = isOuterUser ? 'caseThemes' : 'works';
          const cases = {caseId: key, [caseWorksKey]: caseThemes, docs: uniq(docs)};
          inv.cases.push(cases);
        }
        /*for (let i=0; i < item.length; i++) {
         if (inv.cases !== '') inv.cases += ',';
         inv.cases += item[i].key.split('_')[1];
         }*/
        fund.invs.push(inv);
      }
      datas.push(fund);
    }
    /*readingRoomMakeOrder(userId, archiveId, datas)
      .then(res => {
        if (!res.success) {
          res.errors.forEach(err => {
            message.error(err.text);
          });
          return;
        }
        message.success(t('ORDERS_SUCCESS'));
        clearBasket();
        hideBasket();
      });*/
  };

  const menuProfile = (
    <Menu>
      <Menu.Item key="profile"><Link to='/profile'><Icon type='user'/> {t('PROFILE')} </Link></Menu.Item>
      <Menu.Item key="exit">
        <Link to="/" onClick={props.handleLogout}>
          <Icon type='logout'/> {t('EXIT')}
        </Link>
      </Menu.Item>
    </Menu>
  )

  return (
    <div className="nav-wrapper">
      <Link className="nav-wrapper__link" to="/">
        <span title={t('MAIN')}><Icon style={{fontSize: '17px'}} type="home"/></span>
      </Link>
      {router.pathname === '/uses/readingRoom' &&
        <div className="nav-wrapper__basket" tabIndex={0}>
          <ShowComponent privs={['orderbasket']} component={
            <Badge count={basket.length} onClick={showBasket}>
              <Icon style={{fontSize: '17px'}} type="shopping-cart"/>
            </Badge>}
          />
          {/*<BasketContent
            show={basketIsShown}
            title={'Корзина'}
            user={user}
            tofiConstants={tofiConstants}
            t={t}
            onOk={recieveOrder}
            onCancel={hideBasket}
            basketState={basketState}
            handleListItemClick={handleListItemClick}
            changeCaseInBasket={changeCaseInBasket}
            basket={basket}
            width={'90%'}
          />*/}
        </div>
      }
      <div className="nav-wrapper__lang">
        <Dropdown overlay={langMenu} trigger={['hover']}>
          <Badge count={localStorage.getItem('i18nextLng')}><Icon style={{fontSize: '17px'}} type="global"/></Badge>
        </Dropdown>
      </div>
      <Link className="nav-wrapper__link" to="/help">
        <span title={t('HELP')}><Icon style={{fontSize: '17px'}} type="question-circle-o"/></span>
      </Link>
      <div className="nav-wrapper__profile">
        <Dropdown overlay={menuProfile} trigger={['hover']}>
          <div> {user && user.name} <Icon style={{fontSize: '17px'}} type="user"/></div>
        </Dropdown>
      </div>
    </div>
  );
};

export default LoggedInNav;