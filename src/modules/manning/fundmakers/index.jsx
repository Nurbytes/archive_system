import React, {Component} from 'react';
import AntTabs from "../../../components/tabs/index";
import OrganizationsFM from "./organizations/index";
import IndividualsFM from "./individuals/index";


// Фондообразователи.
class Fundmakers extends Component {
  render() {
    const { t } = this.props;
    return (
      <>
      <AntTabs type="card" tabs={[
        {
          tabKey: 'organizations',
          tabName: t('ORGANIZATIONS'),
          tabContent: <OrganizationsFM
            t={t}
            // cubeForOrgFundmaker={cubeForOrgFundmaker}
            // tofiConstants={tofiConstants}
            // loadOrgFundmaker={this.loadOrgFundmaker}
            // globalDate={globalDate}
          />
        },
        {
          tabKey: 'individuals',
          tabName: t('INDIVIDUALS'),
          tabContent: <IndividualsFM
            t={t}
            // cubeForLPFundmaker={cubeForLPFundmaker}
            // tofiConstants={tofiConstants}
            // globalDate={globalDate}
            // getCube={getCube}
          />
        }
      ]}/>
      </>
    );
  }
}

export default Fundmakers;