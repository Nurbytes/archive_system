import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { store } from "./store";
import ArchiveApp from "./ArchiveApp";
import { HashRouter } from 'react-router-dom';

class App extends Component {
  render() {
    return (
        <Provider store={store}>
          <HashRouter>
            <ArchiveApp />
          </HashRouter>
        </Provider>
    );
  }
}

export default App;
