import React, {Component} from 'react';
import CubeTable from "../../../../components/cubeTable/index";
import {connect} from "react-redux";
import {getPropVal, getPropValWithChilds} from "../../../../store/actions/selectActions";
import {populateMeta} from "../../../../store/actions/tableActions";
import Button from "antd/lib/button";
import Card from "./card/index";
import {destroyCube} from "../../../../store/actions/cubeActions";

// Фондообразователи - юридические лица.
class OrganizationsFM extends Component {

  componentDidMount() {
    const { tofiConstants, populateMeta, lng, outerFilters } = this.props;
    if(!tofiConstants) return;
    const isActiveTrue = tofiConstants.isActiveTrue;
    populateMeta(
      'cubeForOrgFundmaker',
      { outerFilters: { ...outerFilters, 'isActive': [{value: isActiveTrue.id, label: isActiveTrue.name[lng]}]} }
    )
  }

  handleAddNew = () => {
    this.props.populateMeta('cubeForOrgFundmaker', {cardOpen: true});
    this.props.destroyCube('cubeForOrgFundmakerSingle');
  };

  render() {
    if(!this.props.tofiConstants) return null;
    const { lng, t } = this.props;
    return (
      <>
        <CubeTable
          cubeConst='cubeForOrgFundmaker'
          doConds={[{ids: '1001_1063172,1001_1063494,1001_1063495'}]}
          dpConsts='formOfAdmission,legalStatus,fundmakerArchive,orgIndustry,isActive'
          header={{
            addButton: <Button size='small' type="primary" shape='circle' onClick={this.handleAddNew} icon='plus'/>,
            filters: [
              {
                constName: 'formOfAdmission',
                type: 'asyncSelect',
                isSearchable: false,
                getOptions: () => this.props.getPropVal('formOfAdmission'),
                placeholder: t('FORM_OF_ADMISSION')
              },
              {
                constName: 'legalStatus',
                type: 'asyncSelect',
                isSearchable: true,
                getOptions: () => this.props.getPropVal('legalStatus'),
              },
              {
                constName: 'orgIndustry',
                type: 'asyncSelectVirt',
                isSearchable: true,
                getOptions: () => this.props.getPropVal('orgIndustry'),
              },
              {
                constName: 'fundmakerArchive',
                type: 'asyncSelect',
                isSearchable: false,
                getOptions: () => this.props.getPropVal('fundmakerArchive'),
              },
              {
                constName: 'isActive',
                type: 'asyncSelect',
                isSearchable: false,
                getOptions: () => this.props.getPropVal('isActive'),
              }
            ]
          }}
          card={{
            component: <Card t={t}/>
          }}
          columns={[
            {
              key: 'numb',
              title: '№',
              dataIndex: 'numb',
              filter: {
                type: 'string'
              },
              width: '5%'
            },
            {
              key: 'name',
              title: t('NAME'),
              dataIndex: 'name',
              width: '35%',
              filter: {
                type: 'string'
              },
              render: obj => obj && obj[lng]
            },
            {
              key: 'legalStatus',
              dataIndex: 'legalStatus',
              width: '19%',
              render: obj => obj && obj.label
            },
            {
              key: 'formOfAdmission',
              dataIndex: 'formOfAdmission',
              width: '19%',
              render: obj => obj && obj.label
            },
            {
              key: 'orgIndustry',
              dataIndex: 'orgIndustry',
              width: '22%',
              render: val => {
                const value = val && val.sort((a, b) => a.value > b.value ? 1 : -1)[0];
                return value && value.label;
              }
            },
          ]}
        />
      </>
    );
  }
}

function mapState(state) {
  return {
    lng: state.lng,
    tofiConstants: state.handbooks.tofiConstants,
  }
}

export default connect(mapState, { getPropVal, getPropValWithChilds, populateMeta, destroyCube })(OrganizationsFM);