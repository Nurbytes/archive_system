import {handbookRequests} from "../../config";

export const GET_TOFI_CONSTANTS = 'GET_TOFI_CONSTANTS';
export const GET_ACCESS_LEVELS = 'GET_ACCESS_LEVELS';

export const getTofiConstants = () => async dispatch => {
  try {
    const res = await handbookRequests.getAllConstants();
    if(res.success) {
      window.tofiConstants = res.data;
      dispatch({ type: GET_TOFI_CONSTANTS, constants: res.data });
      return {success: true}
    }
    return { success: false };
  } catch {
    return {success: false}
  }
};

export const getAccessLevels = () => async dispatch => {
  try {
    const res = await handbookRequests.getAccessLevels();
    if(res.success) {
      dispatch({ type: GET_ACCESS_LEVELS, accessLevel: res.data });
      return {success: true}
    }
    return { success: false };
  } catch {
    return {success: false}
  }
};