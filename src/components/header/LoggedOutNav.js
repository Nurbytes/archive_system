import React from 'react';
import {Link} from "react-router-dom";
import Dropdown from "antd/lib/dropdown";
import Badge from "antd/lib/badge";
import Icon from "antd/lib/icon";

const LoggedOutNav = (props) => {

  const { t, langMenu } = props;

  return (
    <div className="nav-wrapper">
      <Link className="nav-wrapper__link" to="/"> <span title={t('MAIN')}><Icon style={{ fontSize: '17px' }} type="home" /></span> </Link>
      <div className="nav-wrapper__lang">
        <Dropdown overlay={langMenu} trigger={['hover']}>
          <Badge count={localStorage.getItem('i18nextLng')}><Icon style={{ fontSize: '17px' }} type="global" /></Badge>
        </Dropdown>
      </div>
      <Link className="nav-wrapper__link" to="/login"><span title={t('LOGIN')}><Icon style={{ fontSize: '17px' }} type="login" /></span></Link>
      <Link className="nav-wrapper__link" to="/signup"><span title={t('SIGNUP')}><Icon style={{ fontSize: '17px' }} type="plus-circle" /></span></Link>
      <Link className="nav-wrapper__link" to="/help"> <span title={t('HELP')}><Icon style={{ fontSize: '17px' }} type="question-circle-o" /></span> </Link>
    </div>
  );
};

export default LoggedOutNav;