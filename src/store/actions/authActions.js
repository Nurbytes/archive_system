import {authRequests} from "../../config";

export const SET_USER = 'SET_USER';
export const SIGNUP_SUCCESS = 'SIGNUP_SUCCESS';
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';

export const login = (login, password) => dispatch => {
  return authRequests.login(login, password)
    .then(res => {
      if (res.success) {
        dispatch({
          type: SET_USER,
          user: res.data,
        })
      } else {
        throw res.errors[0].text
      }
    })
};

export const logout = () => dispatch => {
  authRequests.logout()
    .then(() => {
        dispatch({
          type: SET_USER,
          user: null,
        })
      }, console.error);
};

export const onAppLoad = () => dispatch => {
  return authRequests.getUser()
    .then(res => {
      if (res.success) {
        dispatch({
          type: SET_USER,
          user: res.data,
        })
      } else {
        dispatch({
          type: SET_USER,
          user: null,
        })
      }
    })
    .catch(err => {
      console.error(err);
      dispatch({
        type: SET_USER,
        user: null,
      })
    })
};