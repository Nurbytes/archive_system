import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
  renderAsyncSelect,
  renderDPicker,
  renderInput,
  renderInputLang,
  renderSelect, renderUploadButton
} from "../../../../utils/form_components";
import {required, requiredDate, requiredKey, requiredLng} from "../../../../utils/form_validations";
import {digits, normalizePhone} from "../../../../utils/form_normalizing";
import CubeForm from "../../../../components/cubeForm/index";
import {connect} from "react-redux";
import {clearSelectOptions, getPropVal, getPropValWithChilds, loadClsObj} from "../../../../store/actions/selectActions";
import Button from "antd/lib/button";
import {clearFilters} from "../../../../store/actions/cubeActions";
import {getTofiConstByCod} from "../../../../utils/index";

// Основные сведения карточки архивного фонда.
class MainInfoFund extends Component {

  state = {
    clsConst: '',
    fundFeature: ''
  };

  footer = (isSubmitting, errors, reset) => <>
    <Button icon='check' loading={isSubmitting} htmlType='submit' disabled={!!Object.keys(errors).length}>{this.props.t('SAVE')}</Button>
    <Button type='danger' onClick={reset}>{this.props.t('CANCEL')}</Button>
  </>;

  mapFundFM = {
    fundOrg: "fundmakerOrg",
    fundLP: "fundmakerLP",
    collectionOrg: "fundmakerOrg",
    collectionLP: "fundmakerLP",
    jointOrg: "fundmakerOrg",
    jointLP: "fundmakerLP"
  };

  handleSelectChange = (name, value) => {
    this.setState({[name]: value});
    this.props.clearSelectOptions('fundmakerOfIK');
    this.props.clearSelectOptions('fundmakerMulti');
  };

  render() {
    const { t, accessLevelOptions, clearFilters, tofiConstants, lng, loadClsObj } = this.props;
    const { clsConst } = this.state;
    return <CubeForm
      cubeConst='cubeForFundAndIK'
      initialValues={{ accessLevel: 1 }}
      // submitAddition={() => clearFilters('cubeForFundAndIK')}
      // onSubmit={console.log}
      footer={this.footer}
      fields={[
        {propConst: 'fundArchive', component: renderAsyncSelect, getOptions: () => this.props.getPropVal('fundArchive'), validate: requiredKey},
        {
          name: 'clsConst',
          component: renderSelect,
          validate: requiredKey,
          label: t('FUND_TYPE'),
          options: ['fundOrg','fundLP','collectionOrg','collectionLP','jointOrg','jointLP']
            .map(cns => ({value: tofiConstants[cns].id, label: tofiConstants[cns].name[lng]})),
          onSelectChange: this.handleSelectChange
        },
        {name: 'name', component: renderInputLang, validate: requiredLng, label: t('SHORT_NAME')},
        {name: 'fullName', component: renderInputLang, validate: requiredLng, label: t('NAME')},
        {name: 'dbeg', component: renderDPicker, label: t('DBEG')},
        {name: 'dend', component: renderDPicker, label: t('DEND')},
        {name: 'accessLevel', component: renderSelect, validate: requiredKey, label: t('ACCESS_LEVEL'), options: accessLevelOptions},
        {propConst: 'fundNumber', component: renderInput, validate: required},
        {propConst: 'fundIndex', component: renderInput},
        {
          propConst: 'fundmakerOfIK',
          component: renderAsyncSelect,
          virtualized: true,
          getOptions: () => loadClsObj([this.mapFundFM[getTofiConstByCod(`_C_${clsConst}`)]],'fundmakerOfIK'),
          hidden: !clsConst || !['fundOrg', 'fundLP'].includes(getTofiConstByCod(`_C_${clsConst}`))
        },
        {
          propConst: 'fundmakerMulti',
          component: renderAsyncSelect,
          virtualized: true,
          isMulti: true,
          getOptions: () => loadClsObj([this.mapFundFM[getTofiConstByCod(`_C_${clsConst}`)]],'fundmakerMulti'),
          hidden: !clsConst || ['fundOrg', 'fundLP'].includes(getTofiConstByCod(`_C_${clsConst}`))
        },
        {propConst: 'fundCategory', component: renderAsyncSelect, getOptions: () => this.props.getPropVal('fundCategory') },
        {propConst: 'fundFeature', component: renderAsyncSelect, getOptions: () => this.props.getPropVal('fundFeature'), onSelectChange: this.handleSelectChange},
        {propConst: 'fundExitDate', component: renderDPicker, hidden: String(this.state.fundFeature) !== String(tofiConstants.excluded.id)},
        {propConst: 'fundExitReason', component: renderInputLang, hidden: String(this.state.fundFeature) !== String(tofiConstants.excluded.id)},
        {propConst: 'fundIndustry', component: renderAsyncSelect, virtualized: true, getOptions: () => this.props.getPropValWithChilds('fundIndustry'), validate: requiredKey},
        {propConst: 'fundDbeg', component: renderInput},
        {propConst: 'fundDend', component: renderInput},
        {propConst: 'fundFirstDocFlow', component: renderDPicker, validate: requiredDate},
        {propConst: 'fundDateOfLastCheck', component: renderDPicker},
        {propConst: 'collectionCreateDate', component: renderDPicker, validate: requiredDate, hidden: !['collectionOrg', 'collectionLP'].includes(getTofiConstByCod(`_C_${clsConst}`))},
        {propConst: 'creationConds', component: renderInputLang, validate: requiredDate, hidden: !['collectionOrg', 'collectionLP'].includes(getTofiConstByCod(`_C_${clsConst}`))},
        {propConst: 'creationReason', component: renderInputLang, validate: requiredDate, hidden: !['collectionOrg', 'collectionLP'].includes(getTofiConstByCod(`_C_${clsConst}`))},
        {propConst: 'creationPrinciple', component: renderInputLang, validate: requiredDate, hidden: !['collectionOrg', 'collectionLP'].includes(getTofiConstByCod(`_C_${clsConst}`))},
        {propConst: 'collectionLocation', component: renderInputLang, hidden: !['collectionOrg', 'collectionLP'].includes(getTofiConstByCod(`_C_${clsConst}`))},
        {propConst: 'lastReceivedYear', component: renderInput, normalize: digits(4)}
      ]}
    />;
  }
}

MainInfoFund.propTypes = {
  t: PropTypes.func.isRequired
};

function mapState(state) {
  return {
    lng: state.lng,
    tofiConstants: state.handbooks.tofiConstants,
    accessLevelOptions: state.handbooks.accessLevel
      .map(option => ({value: option.id, label: option.name[state.lng]}))
  }
}

export default connect(mapState, {getPropVal, getPropValWithChilds, clearFilters, loadClsObj, clearSelectOptions})(MainInfoFund);