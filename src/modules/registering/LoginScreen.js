import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Formik } from 'formik';
import LoginForm from './LoginForm';
import { login } from '../../store/actions/authActions';
import './login.scss';
import Icon from "antd/lib/icon";

class LoginScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showPassword: false
    };
    this.onSubmit = this.onSubmit.bind(this);
  }

  async onSubmit(values, formMeta) {
    try {
      formMeta.setStatus(null);
      const res = await this.props.login(values.login, values.password);
      this.props.history.push('/');
      formMeta.setSubmitting(false);
      return res;
    } catch(err) {
      console.error('login_error', err);
      formMeta.setSubmitting(false);
      formMeta.setStatus({ submitFailed: true, msg: err })
    }
  }

  handleCheck = () => {
    this.setState({ showPassword: !this.state.showPassword });
  };

  render() {
    const iconEye = this.state.showPassword ? 'lock' : 'eye';
    const suffix = <Icon type={iconEye} style={{ fontSize: '16px' }} onClick={this.handleCheck} />;
    return (
      <div id="loginContainer">
        <Formik
          initialValues={{ login: '', password: '' }}
          onSubmit={this.onSubmit}
          render={props => <LoginForm showPassword={this.state.showPassword} suffix={suffix} {...props}/>}
        />
      </div>
    );
  }
}

LoginScreen.propTypes = {
  login: PropTypes.func.isRequired,
};


export default connect(null, { login })(LoginScreen);
