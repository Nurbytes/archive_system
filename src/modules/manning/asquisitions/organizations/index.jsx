import React, {Component} from 'react';
import CubeTable from "../../../../components/cubeTable/index";
import {connect} from "react-redux";
import {getPropVal} from "../../../../store/actions/selectActions";
import {populateMeta} from "../../../../store/actions/tableActions";
import Button from "antd/lib/button";

// Источники комплектования юридических лиц
class OrganizationsAsquisition extends Component {

    componentDidMount() {
        const { tofiConstants, populateMeta, lng, outerFilters } = this.props;
        if(!tofiConstants) return;
        const isActiveTrue = tofiConstants.isActiveTrue;
        populateMeta(
            'cubeForFundAndIK',
            { outerFilters: { ...outerFilters, 'isActive': [{value: isActiveTrue.id, label: isActiveTrue.name[lng]}]} }
        )
    }

    render() {
        if(!this.props.tofiConstants) return null;
        const { lng, t, populateMeta } = this.props;
        return (
            <>
            <CubeTable
                cubeConst='cubeForFundAndIK'
                //doConds={[{ids: '1001_1063176,1001_1063494,1001_1063495'}]}
                dpConsts='formOfAdmission,legalStatus,fundmakerArchive,orgIndustry,isActive'
                //dpConsts='formOfAdmission,legalStatus,orgIndustry,isActive'
                header={{
                    //addButton: <Button size='small' onClick={() => populateMeta('cubeForOrgFundmaker', {cardOpen: true})}>{t('ADD')}</Button>,
                    filters: [
                        {
                            constName: 'formOfAdmission',
                            type: 'asyncSelect',
                            isMulti: true,
                            isSearchable: false,
                            getOptions: () => this.props.getPropVal('formOfAdmission'),
                            placeholder: t('FORM_OF_ADMISSION')
                        },
                        {
                            constName: 'legalStatus',
                            type: 'asyncSelect',
                            isMulti: true,
                            isSearchable: true,
                            getOptions: () => this.props.getPropVal('legalStatus'),
                        },
                        {
                            constName: 'orgIndustry',
                            type: 'asyncSelectVirt',
                            isMulti: true,
                            isSearchable: true,
                            getOptions: () => this.props.getPropVal('orgIndustry'),
                        },
                        {
                            constName: 'fundmakerArchive',
                            type: 'asyncSelect',
                            isMulti: true,
                            isSearchable: false,
                            getOptions: () => this.props.getPropVal('fundmakerArchive'),
                        },
                        {
                            constName: 'isActive',
                            type: 'asyncSelect',
                            isMulti: true,
                            isSearchable: false,
                            getOptions: () => this.props.getPropVal('isActive'),
                        }
                    ]
                }}
                card={{
                    component: <div>Карточка источника комплектования организации</div>
                }}
                columns={[
                    {
                        key: 'numb',
                        title: '№',
                        dataIndex: 'numb',
                        filter: {
                            type: 'string'
                        },
                        width: '5%'
                    },
                    {
                        key: 'name',
                        title: t('NAME'),
                        dataIndex: 'name',
                        width: '35%',
                        filter: {
                            type: 'string'
                        },
                        render: obj => obj && obj[lng]
                    },
                    {
                        key: 'orgIndustry',
                        dataIndex: 'orgIndustry',
                        width: '22%',
                        render: val => {
                            const value = val && val.sort((a, b) => a.value > b.value ? 1 : -1)[0];
                            return value && value.label;
                        }
                    },
                ]}
            />
            </>
        );
    }
}

function mapState(state) {
    return {
        lng: state.lng,
        tofiConstants: state.handbooks.tofiConstants
    }
}

export default connect(mapState, { getPropVal, populateMeta })(OrganizationsAsquisition);