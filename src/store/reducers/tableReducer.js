import {DESTROY_TABLE, POPULATE_META} from "../actions/tableActions";

export default (state = {}, action) => {
  switch (action.type) {
    // Maybe make it destroy table by cubeConst
    case DESTROY_TABLE:
      return {};
    case POPULATE_META:
      // if there is no table yet fake it empty obj
      const table = state[action.cubeConst] || {};
      return {
        ...state,
        [action.cubeConst]: {
          ...table,
          ...action.meta
        }
      };
    default:
      return state;
  }
}