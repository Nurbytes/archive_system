import React, {Component} from 'react';
import CubeTable from "../../../../components/cubeTable/index";
import {connect} from "react-redux";
import {getPropVal} from "../../../../store/actions/selectActions";
import {populateMeta} from "../../../../store/actions/tableActions";
import Button from "antd/lib/button";

// Фондообразователи - физические лица.
class IndividualsFM extends Component {

    componentDidMount() {
        const { tofiConstants, populateMeta, lng, outerFilters } = this.props;
        if(!tofiConstants) return;
             populateMeta(
            'cubeForLPFundmaker',
            { outerFilters: { ...outerFilters} }
        )
    }

  render() {
      if(!this.props.tofiConstants) return null;
      const { lng, t, populateMeta } = this.props;
    return (
        <>
        <CubeTable
            cubeConst='cubeForLPFundmaker'
            //doConds={[{ids: '1001_1063176,1001_1063494,1001_1063495'}]}
            // dpConsts='formOfAdmission,legalStatus,fundMakerArchive,orgIndustry,isActive'
            dpConsts='personAcademicDegree,personAcademicTitle,personSpecialty'
            header={{
                addButton: <Button size='small' type="primary" shape='circle' onClick={() => populateMeta('cubeForLPFundmaker', {cardOpen: true})} icon="plus"/>,
                filters: [
                    {
                        constName: 'personAcademicDegree',
                        type: 'asyncSelect',
                        isMulti: true,
                        isSearchable: false,
                        getOptions: () => this.props.getPropVal('personAcademicDegree'),
                        //placeholder: t('FORM_OF_ADMISSION')
                    },
                    {
                        constName: 'personAcademicTitle',
                        type: 'asyncSelect',
                        isMulti: true,
                        isSearchable: true,
                        getOptions: () => this.props.getPropVal('personAcademicTitle'),
                    },
                    {
                        constName: 'personSpecialty',
                        type: 'asyncSelectVirt',
                        isMulti: true,
                        isSearchable: true,
                        getOptions: () => this.props.getPropVal('personSpecialty'),
                    }
                ]
            }}
            card={{
                component: <div>Карточка фондообразователя физического лица</div>
            }}
            columns={[
                {
                    key: 'numb',
                    title: '№',
                    dataIndex: 'numb',
                    filter: {
                        type: 'string'
                    },
                    width: '7%'
                },
                {
                    key: 'name',
                    title: t('NAME'),
                    dataIndex: 'name',
                    width: '70%',
                    filter: {
                        type: 'string'
                    },
                    render: obj => obj && obj[lng]
                },
                {
                    key: 'personSpecialty',
                    dataIndex: 'personSpecialty',
                    width: '23%',
                    render: obj => obj && obj.label
                },
            ]}
        />
        </>
    );
  }
}

function mapState(state) {
    return {
        lng: state.lng,
        tofiConstants: state.handbooks.tofiConstants
    }
}

export default connect(mapState, { getPropVal, populateMeta })(IndividualsFM);