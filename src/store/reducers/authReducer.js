import {
  SIGNUP_SUCCESS,
  LOGOUT_SUCCESS,
  SET_USER
} from '../actions/authActions';

export default (state = {user: null}, action) => {
  switch (action.type) {
    case SET_USER:
    case SIGNUP_SUCCESS:
      return {user: action.user};
    case LOGOUT_SUCCESS:
      return {
        user: null
      };
    default:
      return state;
  }
};
