import {Cube} from "./cubeParser";

export function getSingleCubeFilters(ids, cubeConst) {
  return {
    filterDOAnd: [{
      dimConst: Cube[cubeConst][0],
      concatType: "and",
      conds: [{ids}]
    }]
  }
}

// Возвращает наименование ТОФИ-константы по коду экземпляра сущности.
export function getTofiConstByCod(cod) {
  const tofiConst = Object.values(window.tofiConstants).find(val => val.cod == cod);
  return tofiConst && tofiConst.constName;
}