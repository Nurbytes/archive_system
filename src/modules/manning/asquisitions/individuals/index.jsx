import React, {Component} from 'react';
import CubeTable from "../../../../components/cubeTable/index";
import {connect} from "react-redux";
import {getPropVal} from "../../../../store/actions/selectActions";
import {populateMeta} from "../../../../store/actions/tableActions";

// Источники комплектования физических лиц
class IndividualsAsquisition extends Component {

    componentDidMount() {
        const { tofiConstants, populateMeta, lng, outerFilters } = this.props;
        if(!tofiConstants) return;
             populateMeta(
            'cubeForFundAndIK',
            { outerFilters: { ...outerFilters} }
        )
    }

  render() {
      if(!this.props.tofiConstants) return null;
      const { lng, t, populateMeta } = this.props;
    return (
        <>
        <CubeTable
            cubeConst='cubeForFundAndIK'
            dpConsts='personAcademicDegree,personAcademicTitle,personSpecialty'
            header={{
                filters: [
                    {
                        constName: 'personAcademicDegree',
                        type: 'asyncSelect',
                        isMulti: true,
                        isSearchable: false,
                        getOptions: () => this.props.getPropVal('personAcademicDegree'),
                        //placeholder: t('FORM_OF_ADMISSION')
                    },
                    {
                        constName: 'personAcademicTitle',
                        type: 'asyncSelect',
                        isMulti: true,
                        isSearchable: true,
                        getOptions: () => this.props.getPropVal('personAcademicTitle'),
                    },
                    {
                        constName: 'personSpecialty',
                        type: 'asyncSelectVirt',
                        isMulti: true,
                        isSearchable: true,
                        getOptions: () => this.props.getPropVal('personSpecialty'),
                    }
                ]
            }}
            card={{
                component: <div>Карточка источника комплектования физического лица</div>
            }}
            columns={[
                {
                    key: 'numb',
                    title: '№',
                    dataIndex: 'numb',
                    filter: {
                        type: 'string'
                    },
                    width: '7%'
                },
                {
                    key: 'name',
                    title: t('NAME'),
                    dataIndex: 'name',
                    width: '70%',
                    filter: {
                        type: 'string'
                    },
                    render: obj => obj && obj[lng]
                },
                {
                    key: 'personSpecialty',
                    dataIndex: 'personSpecialty',
                    width: '23%',
                    render: obj => obj && obj.label
                },
            ]}
        />
        </>
    );
  }
}

function mapState(state) {
    return {
        lng: state.lng,
        tofiConstants: state.handbooks.tofiConstants
    }
}

export default connect(mapState, { getPropVal, populateMeta })(IndividualsAsquisition);