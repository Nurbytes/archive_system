import React, {Component} from 'react';
import AntTabs from "../../../components/tabs/index";
import OrganizationsAsquisition from "./organizations/index";
import IndividualsAsquisition from "./individuals/index";
import {withNamespaces} from "react-i18next";


// Источники комплектования.
class Asquisitions extends Component {
    render() {
        const { t } = this.props;
        return (
            <AntTabs type="card" tabs={[
                {
                    tabKey: 'organizations',
                    tabName: t('ORGANIZATIONS'),
                    tabContent: <OrganizationsAsquisition
                        t={t}
                        // cubeForOrgFundmaker={cubeForOrgFundmaker}
                        // tofiConstants={tofiConstants}
                        // loadOrgFundmaker={this.loadOrgFundmaker}
                        // globalDate={globalDate}
                    />
                },
                {
                    tabKey: 'individuals',
                    tabName: t('INDIVIDUALS'),
                    tabContent: <IndividualsAsquisition
                        t={t}
                        // cubeForLPFundmaker={cubeForLPFundmaker}
                        // tofiConstants={tofiConstants}
                        // globalDate={globalDate}
                        // getCube={getCube}
                    />
                }
            ]}/>
        );
    }
}

export default withNamespaces('common')(Asquisitions);