import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Funds from './funds';
import InspectionSchedule from './inspectionSchedule';
import Works from './works';
import Reports from './reports';

// Учет и хранение
const Keeping = ({ t }) => {
  return (
    <Switch>
      <Route exact path="/keeping/funds" render={props => <Funds t={t} {...props}/>} />
      <Route exact path="/keeping/inspectionSchedule" render={props => <InspectionSchedule t={t} {...props}/>} />
      <Route exact path="/keeping/works" render={props => <Works t={t} {...props}/>} />
      <Route exact path="/keeping/reports" render={props => <Reports t={t} {...props}/>} />
    </Switch>
  );
};

export default Keeping;