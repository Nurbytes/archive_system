import React from 'react';
import Tabs from 'antd/lib/tabs';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import './tabs.scss';

const TabPane = Tabs.TabPane;

const renderTabs = item => {
  return <TabPane tab={item.tabName} key={item.tabKey} disabled={item.disabled}>{item.tabContent}</TabPane>
};

const AntTabs = props => {
  if(!props.user) return null;
  return (
    <Tabs
      {...props}
    >
      {props.tabs
        .filter(item => !item.hidden && (!item.priv || props.user.privs.includes(item.priv)))
        .map(renderTabs)}
    </Tabs>
  )
};

AntTabs.propTypes = {
  type: PropTypes.string,
  tabs: PropTypes.array.isRequired
};

export default connect(state => ({ user: state.auth.user }))(AntTabs);