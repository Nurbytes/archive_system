import React from 'react';
import Input from 'antd/lib/input';
import Radio from 'antd/lib/radio';

const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

export default class InputLng extends React.Component {
  state = {
    lng: localStorage.getItem('i18nextLng')
  };

  inpRef = React.createRef();
  changeLng = e => {
    this.setState({ lng: e.target.value });
    this.inpRef && this.inpRef.focus();
  };

  handleChange = e => {
    const value = {...this.props.value};
    value[this.state.lng] = e.currentTarget.value;
    this.props.setFieldValue(this.props.name, value);
  };

  componentDidCatch(error, errorInfo) {
    console.error(error, errorInfo);
  }

  render() {
    // eslint:disable-next-line
    const { setFieldValue, value, ...rest } = this.props;
    return <>
      <Input
        {...rest}
        value={value && value[this.state.lng]}
        onChange={this.handleChange}
        ref={inp => this.inpRef = inp}
      />
      <RadioGroup onChange={this.changeLng} value={this.state.lng} name={rest.name}>
        <RadioButton value="kz">kz</RadioButton>
        <RadioButton value="ru">ru</RadioButton>
        <RadioButton value="en">en</RadioButton>
      </RadioGroup>
    </>
  }
};