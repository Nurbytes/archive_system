import React, {Component} from 'react';
import CubeTable from "../../../components/cubeTable";
import {connect} from "react-redux";
import {getPropVal} from "../../../store/actions/selectActions";
import {populateMeta} from "../../../store/actions/tableActions";
import Button from "antd/lib/button";
import moment from "moment";
import {Link} from "react-router-dom";
import Card from "./card/index";

// Архивные фонды.
class Funds extends Component {

  templateRight = () => (
    <Link to={`/archiveFund/inventories/${this.props.selectedRow && this.props.selectedRow.key}`}>
      <Button disabled={!this.props.selectedRow}>{this.props.t('VIEW_INVENTORIES')}</Button>
    </Link>
  )

  render() {
    if(!this.props.tofiConstants) return null;
    const { lng, t, populateMeta} = this.props;
    return (
      <>
        <CubeTable
          cubeConst='cubeForFundAndIK'
          //doConds={[{clss: 'fundOrg,fundLP,collectionOrg,collectionLP,jointOrg,jointLP'}]}
          doConds={[{ids: '1008_1064191, 1008_37163, 1008_37164, 1008_37165'}]}
          // dpConsts='fundArchive,fundNumber,fundIndex,fundDbeg,fundDend,fundCategory,fundType,fundFeature,isActive'
          dpConsts='fundArchive,fundNumber,fundIndex,fundDbeg,fundDend,fundCategory,fundFeature,isActive'
          dtConds={[{ ids: moment().startOf('year').format('YYYYMMDD') + moment().endOf('year').format('YYYYMMDD')}]}
          header={{
            addButton: <Button size='small' type="primary" shape='circle' onClick={() => populateMeta('cubeForFundAndIK', {cardOpen: true})} icon='plus' />,
            templateRight: this.templateRight(),
            filters: [
              {
                constName: 'fundArchive',
                type: 'asyncSelect',
                isSearchable: false,
                getOptions: () => this.props.getPropVal('fundArchive'),
              },
              {
                constName: 'fundCategory',
                type: 'asyncSelect',
                isSearchable: true,
                getOptions: () => this.props.getPropVal('fundCategory'),
              },
              {
                constName: 'fundType',
                type: 'select',
                isSearchable: false,
                options: ['fundOrg', 'fundLP', 'collectionOrg', 'collectionLP', 'jointOrg', 'jointLP']
                    .map(c => ({
                      value: this.props.tofiConstants[c].id,
                      label: this.props.tofiConstants[c].name[lng]
                    })),
                placeholder: t('FUND_TYPE')
              },
              {
                constName: 'fundFeature',
                type: 'asyncSelect',
                isSearchable: false,
                getOptions: () => this.props.getPropVal('fundFeature'),
              }
            ]
          }}
          card={{
            component: <Card t={t}/>
          }}
          columns={[
            {
              key: 'fundNumber',
              dataIndex: 'fundNumber',
              filter: {
                type: 'string'
              },
              width: '8%',
              defaultSortOrder: 'ascent',
              // sorter: (a, b) =>((a.fundNumber).replace(/[^0-9]/g,'')) - ((b.fundNumber).replace(/[^0-9]/g,'')),
            },
            {
              key: 'fundIndex',
              dataIndex: 'fundIndex',
              width: '8%',
              filter: {
                type: 'string'
              },
            },
            {
              key: 'name',
              title: t('NAME'),
              dataIndex: 'fullName',
              filter: {
                type: 'string'
              },
              width: '25%',
              render: obj => obj && obj[lng]
            },
            {
              key: 'fundDbeg',
              dataIndex: 'fundDbeg',
              width: '10%',
              filter: {
                type: 'string'
              },
               //render: obj => obj && obj.format('DD-MM-YYYY')
            },
            {
              key: 'fundDend',
              dataIndex: 'fundDend',
              width: '10%',
              filter: {
                type: 'string'
              },
               //render: obj => obj && obj.format('DD-MM-YYYY')
            },
            {
              key: 'fundCategory',
              dataIndex: 'fundCategory',
              width: '10%',
               render: obj => obj && obj.label
            },
            {
              key: 'fundType',
              title: 'Тип фонда',
              dataIndex: 'fundType',
              width: "13%",
              render: obj => obj && obj.label
            },
          ]}
        />
      </>
    );
  }
}

function mapState(state) {
  const table = state.table.cubeForFundAndIK || {};
  return {
    lng: state.lng,
    tofiConstants: state.handbooks.tofiConstants,
    selectedRow: table.selectedRow,
  }
}

export default connect(mapState, { getPropVal, populateMeta })(Funds);