import {CLEAR_ALL_CUBES} from "./cubeActions";

export const CHANGE_GLOBAL_DATE = 'CHANGE_GLOBAL_DATE';

export const changeGlobalDate = date => dispatch => {
  dispatch({
    type: CHANGE_GLOBAL_DATE,
    date
  });
  // clear all cubes
  dispatch({
    type: CLEAR_ALL_CUBES
  })
};