import { cubeRequests }from '../../config';

export const GET_CUBE_PENDING = 'GET_CUBE_PENDING';
export const GET_CUBE_SUCCESS = 'GET_CUBE_SUCCESS';
export const GET_CUBE_FAIL = 'GET_CUBE_FAIL';

export const DESTROY_CUBE = 'DESTROY_CUBE';
export const CLEAR_FILTERS = 'CLEAR_FILTERS';

export const CLEAR_ALL_CUBES = 'CLEAR_ALL_CUBES';

export const getCube = ({ cubeConst, customKey, filters, nodeWithChilds, dte }) => dispatch => {
  // dispatch pending action
  dispatch({
    type: GET_CUBE_PENDING,
    cubeConst: customKey || cubeConst,
    filters
  });
  // make a request
  cubeRequests.getCube(cubeConst, filters, nodeWithChilds, dte)
    .then(json => {
      dispatch({
        type: GET_CUBE_SUCCESS,
        cubeConst: customKey || cubeConst,
        filters,
        data: json.data
      });
    })
    .catch(err => {
      dispatch({
        type: GET_CUBE_SUCCESS,
        cubeConst: customKey || cubeConst,
        error: err
      });
    })
};

export const destroyCube = key => dispatch => {
  dispatch({
    type: DESTROY_CUBE,
    key
  });
};

export const clearFilters = cubeConst => dispatch => {
  dispatch({
    type: CLEAR_FILTERS,
    cubeConst
  })
};
