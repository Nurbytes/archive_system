import {ADD_SELECT_OPTIONS, CLEAR_SELECT_OPTIONS} from '../actions/selectActions';

export default (state={}, action) => {
  switch (action.type) {
    case ADD_SELECT_OPTIONS: return {
      ...state,
      [action.selectKey]: action.data
    };
    case CLEAR_SELECT_OPTIONS: return {
      ...state,
      [action.selectKey]: null
    };
    default: return state;
  }
}