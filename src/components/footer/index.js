import React from 'react';
import './footer.scss';

const Footer = () => {

  return (
    <footer>
      <p className="footer-text">
        <span>Copyright 2019</span>
        <a href="http://factor.kz/"> ТОО "Компания системных исследований 'Фактор'". </a>
        <span>Все права защищены.</span>
      </p>
    </footer>
  )
};

export default Footer;
