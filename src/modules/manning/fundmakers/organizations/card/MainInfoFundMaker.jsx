import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
  renderAsyncSelect,
  renderDPicker,
  renderInput,
  renderInputLang,
  renderSelect, renderUploadButton
} from "../../../../../utils/form_components";
import {required, requiredKey, requiredLng} from "../../../../../utils/form_validations";
import {normalizePhone} from "../../../../../utils/form_normalizing";
import CubeForm from "../../../../../components/cubeForm/index";
import {connect} from "react-redux";
import {getPropVal, getPropValWithChilds} from "../../../../../store/actions/selectActions";
import Button from "antd/lib/button";
import {clearFilters} from "../../../../../store/actions/cubeActions";

// Основные сведения карточки фондообразователя ө юридического лица.
class MainInfoFundMaker extends Component {

  footer = (isSubmitting, errors, reset) => <>
    <Button icon='check' loading={isSubmitting} htmlType='submit' disabled={!!Object.keys(errors).length}>{this.props.t('SAVE')}</Button>
    <Button type='danger' onClick={reset}>{this.props.t('CANCEL')}</Button>
  </>;

  render() {
    const { t, accessLevelOptions, clearFilters } = this.props;
    return <CubeForm
      cubeConst='cubeForOrgFundmaker'
      initialValues={{ accessLevel: 1 }}
      submitAddition={() => clearFilters('cubeForOrgFundmaker')}
      // onSubmit={console.log}
      footer={this.footer}
      fields={[
        {name: 'name', component: renderInputLang, validate: requiredLng, label: t('SHORT_NAME')},
        {name: 'fullName', component: renderInputLang, validate: requiredLng, label: t('NAME')},
        {name: 'dbeg', component: renderDPicker, label: t('DBEG')},
        {name: 'dend', component: renderDPicker, label: t('DEND')},
        {name: 'accessLevel', component: renderSelect, validate: requiredKey, label: t('ACCESS_LEVEL'), options: accessLevelOptions},
        {propConst: 'contractNumber', component: renderInput, validate: required},
        {propConst: 'legalStatus', component: renderAsyncSelect, getOptions: () => this.props.getPropVal('legalStatus') },
        {propConst: 'formOfAdmission', component: renderAsyncSelect, getOptions: () => this.props.getPropVal('formOfAdmission') },
        {propConst: 'orgDocType', component: renderAsyncSelect, getOptions: () => this.props.getPropVal('orgDocType') },
        {propConst: 'orgIndustry', component: renderAsyncSelect, getOptions: () => this.props.getPropValWithChilds('orgIndustry') },
        {propConst: 'isActive', component: renderAsyncSelect, getOptions: () => this.props.getPropVal('isActive') },
        {propConst: 'fundmakerArchive', component: renderAsyncSelect, getOptions: () => this.props.getPropVal('fundmakerArchive') },
        {propConst: 'orgFunction', component: renderUploadButton },
        {propConst: 'structure', component: renderUploadButton },
        {propConst: 'orgAddress', component: renderInputLang},
        {propConst: 'orgPhone', component: renderInput, placeholder: '+7 (', normalize: normalizePhone},
        {propConst: 'orgFax', component: renderInput, placeholder: '+7 (', normalize: normalizePhone},
        {propConst: 'orgEmail', component: renderInput, type: 'email'},
        {propConst: 'orgFormationDoc', component: renderInputLang},
        {propConst: 'orgReorganizationDoc', component: renderInputLang},
        {propConst: 'orgLiquidationDoc', component: renderInputLang},
      ]}
    />;
  }
}

MainInfoFundMaker.propTypes = {
    t: PropTypes.func.isRequired
};

function mapState(state) {
  return {
    lng: state.lng,
    accessLevelOptions: state.handbooks.accessLevel
      .map(option => ({value: option.id, label: option.name[state.lng]}))
  }
}

export default connect(mapState, {getPropVal, getPropValWithChilds, clearFilters})(MainInfoFundMaker);