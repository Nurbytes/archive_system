import React, {Component} from 'react';
import {connect} from 'react-redux';
import Table from 'antd/lib/table';
import PropTypes from 'prop-types';
import moment from "moment";
import Icon from "antd/lib/icon";
import {getCube} from "../../store/actions/cubeActions";
import {Cube, parseCube, parseForTable} from "../../utils/cubeParser";
import {destroyTable, populateMeta} from "../../store/actions/tableActions";
import FilterInput from "./filterInput";
import CSSTransition from 'react-transition-group/CSSTransition'
import AsyncSelect from "../select/AsyncSelect";
import Select from "../select";

import './cubeTable.scss';
import Button from "antd/lib/button";
import SiderCard from "../siderCard";
import Spin from "antd/lib/spin";
import {getSingleCubeFilters} from "../../utils";

class CubeTable extends Component {

  fetchCube = () => {
    const {cubeConst, doConds, dtConds, customKey, cubeFilters} = this.props;
    const filters = this.props.filters || {
      filterDPAnd: [{
        dimConst: Cube[cubeConst][1],
        concatType: "and",
        conds: [{consts: this.props.dpConsts}]
      }]
    };
    if (doConds) {
      filters.filterDOAnd = [{
        dimConst: Cube[cubeConst][0],
        concatType: "and",
        conds: doConds
      }];
    }
    if (dtConds) {
      filters.filterDTOr = [{
        dimConst: Cube[cubeConst][2],
        concatType: "and",
        conds: dtConds
      }];
    }
    const filtersString = JSON.stringify(filters);
    // fetch cube only if filters are different
    if (cubeFilters !== filtersString) {
      this.props.getCube({cubeConst, customKey, filters: filtersString})
    } else {
      this.populate();
    }
  };

  componentDidMount() {
    const { cubeConst } = this.props;
    if (cubeConst) {
      this.fetchCube();
    }
  }

  componentDidUpdate(prevProps) {
    if (!this.props.loading && prevProps.data !== this.props.data) {
      this.populate();
    }
    if(this.props.data && (prevProps.cubeFilters !== this.props.cubeFilters)) {
      this.fetchCube();
    }
  }

  populate = () => {
    const parsedCube = parseCube(this.props.cubeConst, this.props.data);
    const data = parsedCube.map(this.renderTableData);
    this.props.populateMeta(this.props.cubeConst, {data});
  };

  componentWillUnmount() {
    this.props.destroyTable(this.props.cubeConst);
  }

  renderTableData = (item, idx) => {
    const { accessLevelOptions, lng, dpConsts } = this.props;
    const accessLevelObj = accessLevelOptions && accessLevelOptions.find(al => al.id === item.accessLevel);
    const result = {
      key: item.id,
      numb: idx + 1,
      name: item.name ? item.name : {kz: '', ru: '', en: ''},
      fullName: item.fullName ? item.fullName : {kz: '', ru: '', en: ''},
      clsOrRT: item.clsOrRT,
      dbeg: item.dbeg && moment(item.dbeg).isAfter('1800-01-01') ? moment(item.dbeg) : null,
      dend: item.dend && moment(item.dend).isBefore('3333-12-31') ? moment(item.dend) : null,
      accessLevel: accessLevelObj && {value: accessLevelObj.id, label: accessLevelObj.name[lng]},
    };
    parseForTable(item.props, result, dpConsts.split(','));
    return result;
  };

  renderFilters = ({ type, constName, ...rest }) => {
    const { outerFilters, populateMeta, cubeConst, tofiConstants, lng } = this.props;
    const placeholder = tofiConstants[constName] ? tofiConstants[constName].name[lng] : '';
    if(type === 'asyncSelect') {
      return <AsyncSelect
        key={constName}
        name={constName}
        value={outerFilters[constName]}
        isMulti
        onChange={s => {
          populateMeta(
            cubeConst,
            { outerFilters: { ...outerFilters, [constName]: s} }
          )
        }}
        placeholder={placeholder}
        {...rest}
      />
    }if(type === 'select') {
      return <Select
        key={constName}
        name={constName}
        value={outerFilters[constName]}
        isMulti
        onChange={s => {
          populateMeta(
            cubeConst,
            { outerFilters: { ...outerFilters, [constName]: s} }
          )
        }}
        {...rest}
      />
    }
    if(type === 'asyncSelectVirt') {
      return <AsyncSelect
        key={constName}
        name={constName}
        virtualized
        isMulti
        value={outerFilters[constName]}
        onChange={s => {
          populateMeta(
            cubeConst,
            { outerFilters: { ...outerFilters, [constName]: s} }
          )
        }}
        placeholder={placeholder}
        {...rest}
      />;
    }
  };
  handleRowClick = rec => {
    const { cubeConst, cardOpen, singleCubeFilters, getCube, dtConds, populateMeta} = this.props;
    populateMeta(
      cubeConst,
      { selectedRow: rec }
    );
    if (cardOpen) {
      const filters = getSingleCubeFilters(rec.key, cubeConst);
      if (dtConds) {
        filters.filterDTOr = [{
          dimConst: Cube[cubeConst][2],
          concatType: "and",
          conds: dtConds
        }];
      }
      const filtersString = JSON.stringify(filters);
      // fetch cube only if filters are different
      if (singleCubeFilters !== filtersString) {
        getCube({cubeConst, customKey: `${cubeConst}Single`, filters: filtersString});
      }
    }
  };
  handleRowDoubleClick = rec => {
    const { cardOpen, getCube, cubeConst, singleCubeFilters, populateMeta, card } = this.props;
    if(card && !cardOpen) {
      const filtersString = JSON.stringify(getSingleCubeFilters(rec.key, cubeConst));
      // fetch cube only if filters are different
      if (singleCubeFilters !== filtersString) {
        getCube({cubeConst, customKey: `${cubeConst}Single`, filters: filtersString});
      }
      populateMeta(cubeConst, { cardOpen: true });
    }
  };

  renderCol = obj => obj;
  dataFilters = [];
  cols = this.props.columns.map(col => {
    const { search, tofiConstants, lng } = this.props;
    const filters = {};
    if (col.filter) {
      this.dataFilters.push({
        ...col.filter,
        dataIndex: col.dataIndex,
        render: col.render || this.renderCol
      });
      filters.filterDropdown = <FilterInput
        cubeConst={this.props.cubeConst}
        dataIndex={col.dataIndex}
      />;
      filters.filterIcon = <Icon type="filter" style={{color: search[[col.dataIndex]] ? '#ff9800' : '#aaa'}}/>;
    }
    const title = tofiConstants[col.dataIndex]
      ? tofiConstants[col.dataIndex].name[lng]
      : '';
    return {...filters, title , ...col}
  });
  //Card
  closeCard = () => {
    this.props.populateMeta(this.props.cubeConst, {cardOpen: false});
  };

  render() {
    const {cubeConst, customKey, data, columns,
      singleCubeLoading, card, cardOpen, outerFilters, tableData, search, header, ...rest} = this.props;
    this.filteredData = tableData.filter(item => {
      try {
        // debugger;
        return (this.dataFilters).every(fl => {
          return String(fl.render(item[fl.dataIndex]) || '')
            .toLowerCase()
            .includes(String(search[fl.dataIndex] || '').toLowerCase())
        }) && Object.keys(outerFilters).every(key => {
          return ( outerFilters[key].length === 0 || (item[key] && outerFilters[key].some(p => p.value === item[key].value)));
        })
      } catch (err) {
        console.error(err);
        return true;
      }
    });
    return (
      <div id='cubeTable'>
        {header && <div id="cubeTable__header">
          {header.addButton}
          {header.template}
          {header.filters && <div id="cubeTable__header-filters">
            {header.filters.map(this.renderFilters)}
          </div>}
          {header.templateRight}
        </div>}
        <div id="cubeTable__body">
          <Table
            dataSource={this.filteredData}
            scroll={{y: '100%'}}
            size='small'
            bordered
            columns={this.cols}
            onRow={rec => ({
              onClick: () => this.handleRowClick(rec),
              onDoubleClick: () => this.handleRowDoubleClick(rec)
            })}
            pagination={{pageSize: 20, showQuickJumper: true, showSizeChanger: true}}
            {...rest}
          />
          {card && <CSSTransition
            in={cardOpen}
            timeout={300}
            classNames="card"
            unmountOnExit
          >
            <SiderCard
              closer={<Button type='danger' onClick={this.closeCard} shape="circle" icon="arrow-right"/>}
              pushTable={!!card.pushTable}
              side={card.side || 'right'}
            >
              {singleCubeLoading ? <Spin style={{ position: 'absolute', left: '50%', top: '50%' }}/> : card.component}
            </SiderCard>
          </CSSTransition>}
        </div>
      </div>
    );
  }
}

CubeTable.propTypes = {
  cubeConst: PropTypes.string.isRequired,
  columns: PropTypes.arrayOf(PropTypes.shape({
    filter: PropTypes.shape({
      type: PropTypes.string.isRequired
    })
  })).isRequired,
  doConds: PropTypes.arrayOf(PropTypes.shape()),
  dpConsts: PropTypes.string,
  customKey: PropTypes.string,
  header: PropTypes.shape({
    template: PropTypes.node,
    templateRight: PropTypes.node,
    addButton: PropTypes.node,
    filters: PropTypes.arrayOf(
      PropTypes.shape({
        type: PropTypes.string.isRequired,
        constName: PropTypes.string.isRequired,
        getOptions: PropTypes.func
      })
    )
  }),
  card: PropTypes.shape({
    side: PropTypes.oneOf(['right', 'left']),
    pushTable: PropTypes.bool,
    component: PropTypes.node.isRequired
  })
};

function mapState(state, ownProps) {
  const cube = state.cubes[ownProps.customKey || ownProps.cubeConst];
  const table = state.table[ownProps.cubeConst] || {};
  const singleCube = state.cubes[`${ownProps.cubeConst}Single`];
  return {
    loading: cube && cube.isFetching,
    data: cube && cube.data,
    cubeFilters: cube && cube.filters,
    tofiConstants: state.handbooks.tofiConstants,
    accessLevelOptions: state.handbooks.accessLevel,
    lng: state.lng,
    // singleCube
    singleCubeLoading: singleCube && singleCube.isFetching,
    singleCubeFilters: singleCube && singleCube.filters,
    //  table
    search: table.search || {},
    tableData: table.data || [],
    outerFilters: table.outerFilters || {},
    // card
    cardOpen: table.cardOpen,
  }
}

export default connect(mapState, {getCube, populateMeta, destroyTable})(CubeTable);
