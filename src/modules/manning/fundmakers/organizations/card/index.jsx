import React from 'react';
import AntTabs from '../../../../../components/tabs/index';
import MainInfoFundMaker from "./MainInfoFundMaker";

// Карточка фондообразователя ө юридического лица№
const Card = ({ t }) => {
  return (
    <AntTabs
      tabs={[
        {
          tabKey: 'props',
          tabName: t('MAIN_INFO'),
          tabContent: <MainInfoFundMaker t={t} />
        },
        // {
        //   tabKey: 'Description',
        //   disabled: !initialValues.key,
        //   tabName: t('MANAGING'),
        //   tabContent: <ManagingFormFundMaker tofiConstants={tofiConstants} saveProps={saveProps} t={t} initialValues={initialValues}/>
        // },
        // {
        //   tabKey: 'versions',
        //   disabled: !initialValues.key,
        //   tabName: t('VERSIONS'),
        //   tabContent: <FundMakerContent tofiConstants={tofiConstants} getFMCube={getFMCube} t={t} id={initialValues.key} fundMakerVer={initialValues}/>
        // }
      ]}
    />
  );
};

export default Card;