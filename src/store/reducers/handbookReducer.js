import { GET_TOFI_CONSTANTS, GET_ACCESS_LEVELS } from '../actions/handbookActions';

const initialState = {
  tofiConstants: null,
  accessLevel: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_TOFI_CONSTANTS:
      return {
        ...state,
        tofiConstants: action.constants
      };
    case GET_ACCESS_LEVELS:
      return {
        ...state,
        accessLevel: action.accessLevel
      };
    default: return state;
  }
}