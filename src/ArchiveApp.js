import React, {Component, Suspense, lazy} from 'react';
import Layout from "antd/lib/layout";
import Breadcrumb from "antd/lib/breadcrumb";
import message from 'antd/lib/message';
import {connect} from "react-redux";
import {withNamespaces} from "react-i18next";
import {Route, Switch, withRouter} from "react-router-dom";
import Sider from "./components/sider";
import Header from './components/header';
import Main from "./modules/main";
import LoginScreen from "./modules/registering/LoginScreen";
import {getAccessLevels, getTofiConstants} from "./store/actions/handbookActions";
import {onAppLoad} from "./store/actions/authActions";
import Footer from "./components/footer";
import DPickerTOFI from "./components/DPickerTOFI";
import {changeGlobalDate} from "./store/actions/globalDateActions";
import './App.scss';

const Keeping = lazy(() => import('./modules/keeping'));
const Manning = lazy(() => import('./modules/manning'));
const SRA = lazy(() => import('./modules/sra'));
const Using = lazy(() => import('./modules/using'));
const Managing = lazy(() => import('./modules/managing'));
const Administrating = lazy(() => import('./modules/administrating'));

class ArchiveApp extends Component {
  async componentDidMount() {
    // if there is no lng in localstorage set it
    !localStorage.getItem('i18nextLng') && localStorage.setItem('i18nextLng', 'ru');
    const hideLoading = message.loading('Загрузка констант', 0);
    try {
      const [cs, al] = await Promise.all([
        this.props.getTofiConstants(),
        this.props.getAccessLevels()
      ]);
      //this.props.onAppLoad();
      hideLoading();
      if (cs.success && al.success) return;
      message.error('Ошибка загрузки констант');
    } catch (err) {
      hideLoading();
      message.error('Ошибка загрузки констант');
    }
  }


  render() {
    const {user, t, tofiConstants, globalDate, changeGlobalDate} = this.props;
    return (
      <Layout style={{display: 'flex', flexDirection: 'column', height: '100vh'}}>
        <Header location={this.props.location}/>
        <Layout style={{flex: 1}}>
          {user && <Sider t={t} privs={user.privs}/>}
          {tofiConstants && <Layout style={{marginLeft: '5px'}}>
            <div className='subheader'>
              <Breadcrumb style={{margin: '8px 0'}}>
                <Breadcrumb.Item>Home</Breadcrumb.Item>
                <Breadcrumb.Item>List</Breadcrumb.Item>
                <Breadcrumb.Item>App</Breadcrumb.Item>
              </Breadcrumb>
              <DPickerTOFI value={globalDate} onChange={changeGlobalDate}/>
            </div>
            <Layout.Content style={{
              flex: 1,
              padding: 3,
              background: '#fff',
              display: 'flex',
              flexDirection: 'column',
              position: 'relative'
            }}>
              <Suspense fallback={<p>loading</p>}>
                <Switch>
                  <Route exact path="/" component={Main}/>
                  <Route exact path="/login" component={LoginScreen}/>
                  {/*<Route exact path="/signup" component={AsyncSignupForm} />*/}
                  <Route path="/manning" render={props => <Manning t={t} {...props}/>}/>
                  <Route path="/keeping" render={props => <Keeping t={t} {...props}/>}/>
                  <Route path="/sra" render={props => <SRA t={t} {...props}/>}/>
                  <Route path="/using" render={props => <Using t={t} {...props}/>}/>
                  <Route path="/managing" render={props => <Managing t={t} {...props}/>}/>
                  <Route path="/administrating" render={props => <Administrating t={t} {...props}/>}/>
                </Switch>
              </Suspense>
            </Layout.Content>
          </Layout>}
        </Layout>
        <Footer />
      </Layout>
    );
  }
}

function mapState(state) {
  return {
    user: state.auth.user,
    tofiConstants: state.handbooks.tofiConstants,
    globalDate: state.globalDate
  }
}

const connected = connect(mapState, { getTofiConstants, getAccessLevels, onAppLoad, changeGlobalDate })(ArchiveApp);
const withRouteProps = withRouter(connected);

export default withNamespaces(['common'])(withRouteProps);