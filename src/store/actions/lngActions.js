export const CHANGE_LNG = 'CHANGE_LNG';

export const changeLng = lng => dispatch => {
  localStorage.setItem('i18nextLng', lng);
  dispatch({
    type: CHANGE_LNG,
    lng
  })
};