import React from 'react';
import PropTypes from 'prop-types';
import {Field, Formik} from 'formik';
import Form from "antd/lib/form";
import message from 'antd/lib/message';
import {connect} from "react-redux";
import pickBy from 'lodash/pickBy';
import isEqual from 'lodash/isEqual';

import './cubeForm.scss';
import {onCreateObj, parseCube, parseForTable, saveProps} from "../../utils/cubeParser";
import moment from "moment";
import {withNamespaces} from "react-i18next";
import {getSingleCubeFilters} from "../../utils";
import {cubeRequests} from "../../config";
import {getCube} from "../../store/actions/cubeActions";
import {renderInputLang, renderUploadButton} from "../../utils/form_components";
import {getTofiConstByCod} from "../../utils/index";

function renderFields({propConst, component, customComponent, tofiConstants, lng, ...rest }, idx) {
  // if the field is hidden by main component
  if(rest.hidden) return null;
  /*
  *  hide filed by condition
  *  in array goes $AND conditions [{}, {}] = 1 and 2
  *  in object itself goes $OR condition {key: value, key2: value2} = 1 or 2
  * */
  if (rest.hide) {
    const hidden = rest.hide.every(hideCondition =>
      Object.entries(hideCondition).some(([key, value]) => JSON.stringify(rest.values[key]) === JSON.stringify(value))
    );
    if (hidden) return null;
  }
  if (customComponent) return customComponent;
  return (!propConst || tofiConstants[propConst]) && <Field
    key={idx}
    name={propConst}
    placeholder={tofiConstants[propConst] && tofiConstants[propConst].name[lng]}
    label={tofiConstants[propConst] && tofiConstants[propConst].name[lng]}
    formItemLayout={
      {
        labelCol: { span: 10 },
        wrapperCol: { span: 14 }
      }
    }
    formItemClass={`label-left ${rest.validate ? ' required' : ''}`}
    {...rest}
    component={component}
  />
}

class CubeForm extends React.Component {
    state = {
      initialValues: {}
    };
  componentDidCatch(error, errorInfo) {
    console.error(error, errorInfo);
  }

  componentDidMount() {
    if (this.props.cubeData) {
      const parsedCube = parseCube(this.props.cubeConst, this.props.cubeData.data);
      const initialValues = this.renderFormData(parsedCube[0]);
      return this.setState({ initialValues });
    }
    const initialValues = this.props.initialValues || {};
    this.setState({ initialValues });
  }

  componentDidUpdate(prevProps) {
    const { cubeData, cubeConst } = this.props;
    if (prevProps.cubeData !== cubeData) {
      if (!cubeData) {
        const initialValues = this.props.initialValues || {};
        return this.setState({ initialValues });
      }
      const parsedCube = parseCube(cubeConst, cubeData.data);
      const initialValues = this.renderFormData(parsedCube[0]);
      this.setState({ initialValues });
    }
  }

  renderFormData = item => {
    try {
      const { accessLevelOptions, lng, fields } = this.props;
      const fieldsWithLng = [];
      const propConsts = fields
        .filter(field => field.hasOwnProperty('propConst'))
        .map(field => {
          (field.component === renderInputLang /*|| field.component === renderTextareaLang*/) && fieldsWithLng.push(field.propConst);
          return field.propConst
        });
      const accessLevelObj = accessLevelOptions && accessLevelOptions.find(al => al.id === item.accessLevel);
      const result = {
        key: item.id,
        name: item.name ? item.name : {kz: '', ru: '', en: ''},
        fullName: item.fullName ? item.fullName : {kz: '', ru: '', en: ''},
        dbeg: item.dbeg && moment(item.dbeg).isAfter('1800-01-01') ? moment(item.dbeg) : null,
        dend: item.dend && moment(item.dend).isBefore('3333-12-31') ? moment(item.dend) : null,
        accessLevel: accessLevelObj && {value: accessLevelObj.id, label: accessLevelObj.name[lng]},
      };
      parseForTable(item.props, result, propConsts);
      fieldsWithLng.forEach(propConst => {
        result[propConst] = result[propConst + 'Lng']
      });
      return result;
    } catch (e) {
      message.error('Ошибка формы');
      console.error(e);
      return {};
    }
  };

  onSubmit = async (values, meta) => {
    try {
      const { fields, submitAddition, t, clsConst, cubeData, cubeConst } = this.props;
      const cube = { cubeSConst: cubeConst };
      // get changed values only
      const changedVals = pickBy(values, (val, key) => !isEqual(val, this.state.initialValues[key]));
      // sort object's fields, props and files
      const obj = {clsConst};
      const props = {};
      const oFiles = {};
      Object.entries(changedVals).forEach(([key, val]) => {
        // find the field in fields props passed down
        const field = fields.find(fld => key === (fld.propConst || fld.name));
        if (field.name) {
          if(field.name === 'clsConst') return obj[key] = getTofiConstByCod(`_C_${val.value}`);
          return obj[key] = val;
        }
        if (field.component === renderUploadButton) {
          return oFiles[key] = val;
        }
        props[key] = val;
      });
      let filtersString;
      // if there is no values.key then create;
      if (!values.key) {
        const createdObj = await onCreateObj({cube, obj}, t);
        filtersString = JSON.stringify(getSingleCubeFilters(createdObj.data.idItemDO, cubeConst));
        // get cube if there is no yet
        if (!cubeData) {
          const res = await cubeRequests.getCube(cubeConst, filtersString);
          cube.data = res.data;
        } else {
          cube.data = cubeData.data;
        }
        await saveProps({ cube, obj: { doItem: createdObj.data.idItemDO } }, {values: props, oFiles}, t );
        this.props.getCube({cubeConst, customKey: `${cubeConst}Single`, filters: filtersString});
        submitAddition && submitAddition();
        return meta.setSubmitting(false)
      }
      filtersString = JSON.stringify(getSingleCubeFilters(values.key, cubeConst));
      cube.data = cubeData.data;
      await saveProps({ cube, obj: { doItem: values.key } }, {values: props, oFiles}, t, obj );
      this.props.getCube({cubeConst, customKey: `${cubeConst}Single`, filters: filtersString});
      submitAddition && submitAddition();
    } catch (e) {
      console.error(e);
      meta.setSubmitting(false)
    }
  };

  render() {
    const { onSubmit, fields, tofiConstants, lng, footer } = this.props;
    return (
      <Formik
        initialValues={this.state.initialValues}
        enableReinitialize
        onSubmit={onSubmit || this.onSubmit}
        render={({ errors, status, touched, isSubmitting, handleSubmit, handleReset, dirty, values}) => (
          <Form className={`${dirty && 'dirty'}`} onReset={handleReset} onSubmit={handleSubmit}>
            {fields.map((field, idx) => renderFields({...field, tofiConstants, lng, values}, idx))}
              {dirty && <div className='footer'>{footer(isSubmitting, errors, handleReset)}</div>}
          </Form>
        )}
      />
    )
  }
}

CubeForm.propTypes = {
  fields: PropTypes.arrayOf(
    PropTypes.shape({
      propConst: PropTypes.string,
      placeholder: PropTypes.string,
      validate: PropTypes.func,
      component: PropTypes.func.isRequired
    }).isRequired
  ).isRequired,
  submitAddition: PropTypes.func,
  onSubmit: PropTypes.func,
  footer: PropTypes.func.isRequired
};

function mapState(state, ownProps) {
  return {
    lng: state.lng,
    tofiConstants: state.handbooks.tofiConstants,
    accessLevelOptions: state.handbooks.accessLevel,
    cubeData: state.cubes[ownProps.cubeConst + 'Single']
  }
}

export default connect(mapState, {getCube})(withNamespaces()(CubeForm));
