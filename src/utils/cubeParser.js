import moment from 'moment';
import message from 'antd/lib/message';
import map from "lodash/map";
import forEach from "lodash/forEach";
import uuid from "uuid";
import {cubeRequests, SYSTEM_LANG_ARRAY} from '../config';

// 1-doConst; 2-dpConst; 3-dtConst, 4-Константа класса объектов по умолчанию;
export const Cube = {
  cubeForOrgFundmaker: ['doForOrgFundmakers', 'dpForOrgFundmakers', 'fundmakerOrg'],
  cubeForLPFundmaker: ['doForLPFundmakers', 'dpForLPFundmakers'],
  cubeForFundAndIK: ['doForFundAndIK', 'dpForFundAndIK', 'dtForFundAndIK'],
};

const oneLevelCopy = object => ({...object});

export const parseCube = (cubeConst, cubeData) => {
  try {
    const cubeVal = cubeData.cube;
    const [doId, dpId] = Cube[cubeConst].map(c => window.tofiConstants[c].id);
    const dpTable = cubeData[`dp_${dpId}`];
    const doTable = cubeData[`do_${doId}`];
    const doTableWithProps = doTable.map(item => ({ ...item, props: dpTable.map(oneLevelCopy) }));
    cubeVal.forEach(cubeValItem => {
      const prop = doTableWithProps
        .find(doItem => doItem['id'] === cubeValItem[`do_${doId}`])['props']
        .find(dpItem => dpItem['id'] === cubeValItem[`dp_${dpId}`]);
      const propType = prop['typeProp'];
      if(prop.isUniq === 2) {
        if(!prop.idDataPropVal) prop.idDataPropVal = [];
        if(!prop.complexMultiValues) prop.complexMultiValues = {};
        cubeValItem['idDataPropVal'] && prop.idDataPropVal.push(cubeValItem['idDataPropVal']);
      } else {
        prop.idDataPropVal = cubeValItem['idDataPropVal'];
      }
      if(cubeValItem['parentDataPropVal'] && !prop.complexChildValues) prop.complexChildValues = {};
      switch (true) {
        case (propType === 1) :
          return;
        case (propType === 11 && prop.isUniq === 2):
          if(cubeValItem['idRef']) {
            if(!prop.values) prop.values = [];
            prop.values.push({value: cubeValItem['idRef'], label: (cubeValItem['name'][localStorage.getItem('i18nextLng')] || '')})
          }
          break;
        case (propType === 11) :
          if(cubeValItem['parentDataPropVal']) prop.complexChildValues[cubeValItem['parentDataPropVal']] = {
            value: cubeValItem['name'] ? cubeValItem['name'][localStorage.getItem('i18nextLng')] : '',
            refId: cubeValItem['idRef']
          };
          prop.value = cubeValItem['name'] ? cubeValItem['name'][localStorage.getItem('i18nextLng')] : '';
          prop.refId = cubeValItem['idRef'];
          break;
        case (propType === 21) :
        case (propType === 22) :
          if(cubeValItem['parentDataPropVal']) prop.complexChildValues[cubeValItem['parentDataPropVal']] = {
            value: cubeValItem['valueNumb']
          };
          prop.value = cubeValItem['valueNumb'];
          break;
        case (propType.toString().startsWith('31')) :
          switch (propType % 10) {
            case 1:
            case 5:
              if(prop.isUniq === 2) {
                if(!prop.values) prop.values = [];
                cubeValItem['valueStr'] && prop.values.push(cubeValItem['valueStr'][localStorage.getItem('i18nextLng')]);
              } else {
                if(cubeValItem['parentDataPropVal']) prop.complexChildValues[cubeValItem['parentDataPropVal']] = {
                  value: cubeValItem['valueStr']
                };
                prop.value = cubeValItem['valueStr'] ? cubeValItem['valueStr'][localStorage.getItem('i18nextLng')] : '';
                prop.valueLng = cubeValItem['valueStr'] ? cubeValItem['valueStr'] : null;
              }
              break;
            case 2:
            case 3:
            case 4:
              if(cubeValItem['parentDataPropVal']) prop.complexChildValues[cubeValItem['parentDataPropVal']] = {
                value: moment(cubeValItem['valueDateTime'], 'YYYY-MM-DD').isValid() ?
                  moment(cubeValItem['valueDateTime'], 'YYYY-MM-DD').format('DD-MM-YYYY') : ''
              };
              const date = moment(cubeValItem['valueDateTime'], 'YYYY-MM-DD');
              prop.value = date.isValid() ? date.format('DD-MM-YYYY') : '';
              break;
            case 7:
              if(prop.isUniq === 2) {
                if(!prop.values) prop.values = [];
                if(cubeValItem['valueFile']) {
                  const id = cubeValItem['valueFile'][localStorage.getItem('i18nextLng')];
                  const f = new File([id], id);
                  f.uid = `rc-upload-${id}`;
                  prop.values.push(f);
                  if(cubeValItem['parentDataPropVal']) {
                    if(!prop.complexMultiValues[cubeValItem['parentDataPropVal'] + '_' + cubeValItem[`dp_${dpId}`]])
                      prop.complexMultiValues[cubeValItem['parentDataPropVal'] + '_' + cubeValItem[`dp_${dpId}`]] = [];
                    prop.complexMultiValues[cubeValItem['parentDataPropVal'] + '_' + cubeValItem[`dp_${dpId}`]].push(f);
                    prop.complexChildValues[cubeValItem['parentDataPropVal']] = {
                      values: prop.complexMultiValues[cubeValItem['parentDataPropVal'] + '_' + cubeValItem[`dp_${dpId}`]]
                    }};
                }
              } else if(cubeValItem['valueFile']){
                const id = cubeValItem['valueFile'][localStorage.getItem('i18nextLng')];
                const f = new File([id], id);
                f.uid = `rc-upload-${id}`;
                prop.value = f;
                if(cubeValItem['parentDataPropVal']) prop.complexChildValues[cubeValItem['parentDataPropVal']] = {
                  value: f
                };
              }
              break;
            default: break;
          }
          break;
        case (propType === 41 && prop.isUniq === 2):
          if(cubeValItem['idRef']) {
            if(!prop.values) prop.values = [];
            cubeValItem['idRef'] && prop.values.push({value: cubeValItem['idRef'], label: (cubeValItem['name'][localStorage.getItem('i18nextLng')] || '')})
          }
          break;
        case (propType === 41) :
          if(cubeValItem['parentDataPropVal']) prop.complexChildValues[cubeValItem['parentDataPropVal']] = {
            value: cubeValItem['name'] ? cubeValItem['name'][localStorage.getItem('i18nextLng')] : '',
            cube: cubeValItem
          };
          prop.value = cubeValItem['name'] ? cubeValItem['name'][localStorage.getItem('i18nextLng')] : '';
          prop.cube = cubeValItem;
          break;
        case (propType === 51) :
          prop.value = 'relObj';
          break;
        case (propType === 61) :
          prop.value = 'measure';
          break;
        case (propType === 71 && prop.isUniq === 2):
          if(cubeValItem['valueStr']) {
            if(!prop.values) prop.values = [];
            cubeValItem['valueStr'] && prop.values.push({value: cubeValItem['valueStr'], id: (cubeValItem['idDataPropVal'] || '')})
          }
          break;
        case (propType === 71) :
          prop.value = cubeValItem['valueStr'] ? cubeValItem['valueStr'][localStorage.getItem('i18nextLng')] : '';
          prop.valueLng = cubeValItem['valueStr'] ? cubeValItem['valueStr'] : null;
          break;
        default:
          return;
      }
    });
    return doTableWithProps;
  } catch(err) {
    console.warn(err);
    return []
  }
};

/*
* props - all dp of one do
* result - object
* */
export const parseForTable = (props, result, constArr) => {
  const keys = constArr; /* ? constArr : Object.keys(window.tofiConstants);*/
  try {
    props.forEach(dp => {
      const c = keys.find(c => window.tofiConstants[c].cod === `_P_${dp.prop}`);
      if (c) {
        if (dp.isUniq === 1) {
          switch (dp.typeProp) {
            case 11: {
              result[c] = dp.refId ? {value: dp.refId, label: dp.value} : null;
              break;
            }
            case 312: {
              result[c] = dp.value ? moment(dp.value, 'DD-MM-YYYY') : null;
              break;
            }
            case 41: {
              result[c] = dp.cube && dp.cube.idRef ? {value: dp.cube.idRef, label: dp.value} : null;
              break;
            }
            default: {
              result[c] = dp.value ? dp.value : '';
              result[c + 'Lng'] = dp.valueLng ? dp.valueLng : {kz: '', ru: '', en: ''};
              break;
            }
          }
        } else if (dp.isUniq === 2) {
          switch (dp.typeProp) {
            default:
              result[c] = dp.values ? dp.values : [];
          }
        }
      }
    });
  } catch (e) {
    console.warn(e);
    console.log('No such constants', constArr.filter(c => !window.tofiConstants[c]));
  }
};

export const getPropMeta = (cubeProps, cnst) => {
  try {
    return cubeProps.find(prop => prop.prop === cnst.id);

  } catch (err) {
    console.error(cubeProps, cnst, err)
  }
};


export const onCreateObj = async ({cube, obj}, t) => {
  let hideCreateObj;
  try {
    if (!obj.clsConst) obj.clsConst = Cube[cube.cubeSConst][2];
    hideCreateObj = message.loading(t('CREATING_NEW_OBJECT'), 0);
    const res = await cubeRequests.createObj(cube, obj);
    hideCreateObj();
    if (!res.success) {
      res.errors.forEach(err => {
        message.error(err.text)
      });
      return Promise.reject(res)
    }
    message.success(t('CREATE_SUCCESS'));
    return res;
  } catch (e) {
    typeof hideCreateObj === 'function' && hideCreateObj();
    message(t('CREATE_ERROR'));
    console.error(e);
    throw new Error(e);
  }
};

/*
* required c.cube.cubeSConst c.cube.data
* c.obj: doItem
* */
export const saveProps = async (c, v, t, objData) => {
  let hideLoading;
  try {
    if(!c.cube.doConst) c.cube.doConst = Cube[c.cube.cubeSConst][0];
    if(!c.cube.dpConst) c.cube.dpConst = Cube[c.cube.cubeSConst][1];

    hideLoading = message.loading(t('UPDATING_PROPS'), 0);
    const resSave = await onSaveCubeData(c, v, objData);
    hideLoading();
    if (!resSave.success) {
      message.error(t('PROPS_UPDATING_ERROR'));
      resSave.errors.forEach(err => {
        message.error(err.text)
      });
      return Promise.reject(resSave);
    }
    message.success(t('PROPS_SUCCESSFULLY_UPDATED'));
    return resSave;
  }
  catch (e) {
    typeof hideLoading === 'function' && hideLoading();
    message.error(t('PROPS_UPDATING_ERROR'));
    console.error(e);
    throw new Error(e);
  }
};

/*
* Required:
* cubeRequests: data, dpConst, doConst, cubeSConst
* obj: doItem
*
* tofiConstants: tofiConstants
* */

/*
* Structure
* complex: {
*   constant: {
*     mode,
*     values
*   }
* }
*
* values: {constant, constant}
*
* */

export function onSaveCubeData(
  {cube, obj},
  {values, complex, oFiles={}, qFiles={}},
  objData={},
  periods,
  dte=moment().format('YYYY-MM-DD')
) {
  const tofiConstants = window.tofiConstants;
  const files = {...oFiles};
  forEach(qFiles, (val, key) => {
    files[`__Q__${key}`] = val;
  });

  const valuesArr = map(values, buildProps);
  const complexArr = map(complex, ({values, mode}, key) => {
    const propMetaData = getPropMeta(cube.data["dp_" + tofiConstants[cube.dpConst].id], tofiConstants[key]);
    const id = uuid();
    const value = {};
    SYSTEM_LANG_ARRAY.forEach(lang => {
      value[lang] = id;
    });
    return {
      propConst: key,
      val: value,
      typeProp: '71',
      periodDepend: String(propMetaData.periodDepend),
      isUniq: String(propMetaData.isUniq),
      mode,
      child: map(values, buildProps)
    }
  });

  const datas = [{
    own: [{doConst: cube.doConst, doItem: obj.doItem, isRel: "0", objData}],
    props: [...valuesArr, ...complexArr],
    periods: periods || [{ periodType: '0', dbeg: '1800-01-01', dend: '3333-12-31' }]
  }];

  function buildProps(val, key) {
    const propMetaData = getPropMeta(cube.data["dp_" + tofiConstants[cube.dpConst].id], tofiConstants[key]);
    let value = val;
    try {
      if((propMetaData.typeProp === 315 || propMetaData.typeProp === 311 || propMetaData.typeProp === 317) && typeof val === 'string') value = {kz: val, ru: val, en: val};
      if(propMetaData.typeProp === 312) value = moment(val).format('YYYY-MM-DD');
      if(val && typeof val === 'object' && val.value) value = String(val.value);
      if(val && typeof val === 'object' && val.mode) propMetaData.mode = val.mode;
      if(propMetaData.isUniq === 2 && val[0] && val[0].value) {
        propMetaData.mode = val[0].mode;
        value = val.map(v => String(v.value)).join(",");
      }
    } catch (e) {
      console.warn(key,val);
      console.warn(e);
      return;
    }
    return {
      propConst: key,
      val: value,
      typeProp: String(propMetaData.typeProp),
      periodDepend: String(propMetaData.periodDepend),
      isUniq: String(propMetaData.isUniq),
      mode: propMetaData.mode
    }
  }

  return cubeRequests.updateCubeData(cube.cubeSConst, dte, JSON.stringify(datas), files)
}
/*export function onSaveCubeData2(
  {cubeRequests, obj},
  {values, complex, oFiles={}, qFiles={}},
  tofiConstants,
  objData={},
  periods,
  dte=moment().format('YYYY-MM-DD'),
  options={}) {
  const files = {...oFiles};
  forEach(qFiles, (val, key) => {
    files[`__Q__${key}`] = val;
  });

  const valuesArr = map(values, buildProps);
  const complexArr = map(complex, ({values, mode}, key) => {
    const propMetaData = getPropMeta(cubeRequests.data["dp_" + tofiConstants[cubeRequests.dpConst].id], tofiConstants[key]);
    const id = uuid();
    const value = {};
    SYSTEM_LANG_ARRAY.forEach(lang => {
      value[lang] = id;
    });
    return {
      propConst: key,
      val: value,
      typeProp: '71',
      periodDepend: String(propMetaData.periodDepend),
      isUniq: String(propMetaData.isUniq),
      mode,
      child: map(values, buildProps)
    }
  });

  const datas = [{
    own: [{doConst: cubeRequests.doConst, doItem: obj.doItem, isRel: "0", objData}],
    props: [...valuesArr, ...complexArr],
    periods: periods || [{ periodType: '0', dbeg: '1800-01-01', dend: '3333-12-31' }]
  }];

  function buildProps(val, key) {
    const propMetaData = getPropMeta(cubeRequests.data["dp_" + tofiConstants[cubeRequests.dpConst].id], tofiConstants[key]);
    let value = val;
    try {
      if((propMetaData.typeProp === 315 || propMetaData.typeProp === 311 || propMetaData.typeProp === 317) && typeof val === 'string') value = {kz: val, ru: val, en: val};
      if(propMetaData.typeProp === 312) value = moment(val).format('YYYY-MM-DD');
      if(val && typeof val === 'object' && val.value) value = String(val.value);
      if(val && typeof val === 'object' && val.mode) propMetaData.mode = val.mode;
      if(propMetaData.isUniq === 2 && val[0] && val[0].value) {
        propMetaData.mode = val[0].mode;
        value = val.map(v => String(v.value)).join(",");
      }
    } catch (e) {
      console.warn(key,val);
      console.warn(e);
      return;
    }
    return {
      propConst: key,
      val: value,
      periodType: periodType,
      dBeg: dBeg,
      dEnd: dEnd,
      mode: propMetaData.mode
    }
  }

  return updateCubeData2(cubeRequests.cubeSConst, dte, JSON.stringify(datas), options, files)
}*/
