import { CHANGE_LNG } from '../actions/lngActions';

export default (state='ru', {type, lng}) => {
  switch(type) {
    case CHANGE_LNG: return lng;
    default: return state;
  }
}