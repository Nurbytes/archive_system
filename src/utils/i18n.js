import i18n from 'i18next';
import XHR from 'i18next-xhr-backend';

i18n
  .use(XHR)
  .init({
    debug: process.env.NODE_ENV === 'development',
    ns: ['common'],
    defaultNS: 'common',
    fallbackNS: 'common',
    nonExplicitWhitelist: true,
    lng: 'ru', // 'kz' | 'en' | 'ru'
    fallbackLng: 'ru',
    backend: {
      loadPath: process.env.NODE_ENV === 'development'
        ? '/locales/{{lng}}/{{ns}}.json'
        : `/${process.env.REACT_APP_APP_NAME}/client/locales/{{lng}}/{{ns}}.json`
    },
    react: {
      wait: true
    }
  });
export default i18n;