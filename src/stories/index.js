import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import { Button, Welcome } from '@storybook/react/demo';
import Form from "antd/lib/form";
import Input from "antd/lib/input";

storiesOf('Welcome', module).add('to Storybook', () => <Welcome showApp={linkTo('Button')} />);

storiesOf('Button', module)
  .add('with text', () => <Button onClick={action('clicked')}>Hello Button</Button>)
  .add('with some emoji', () => (
    <Button onClick={action('clicked')}>
      <span role="img" aria-label="so cool">
        😀 😎 👍 💯
      </span>
    </Button>
  ));
storiesOf('Form', module)
  .add('Formik', () => (
    <Form onSubmit={e => {
      e.preventDefault();
      action('submit')();
    }}>
      <Form.Item>asd</Form.Item>
      <Form.Item><Input /></Form.Item>
      <Form.Item><Button htmlType='submit'>submit</Button></Form.Item>
    </Form>
  ));