import {entityRequest as entity} from "../../config";
import moment from "moment";

export const ADD_SELECT_OPTIONS = 'ADD_SELECT_OPTIONS';
export const CLEAR_SELECT_OPTIONS = 'CLEAR_SELECT_OPTIONS';

function addSelectOptions(selectKey, data) {
  return {
    type: ADD_SELECT_OPTIONS,
    selectKey,
    data
  }
}

export const getPropVal = propConst => dispatch => {
  return entity.getPropValByConst(propConst)
    .then(json => {
      dispatch(addSelectOptions(propConst, json.data));
      return json
    })
};
export const getPropValWithChilds = propConst => dispatch => {
  return entity.getPropValWithChilds(propConst)
    .then(json => {
      dispatch(addSelectOptions(propConst, json.data));
      return json
    })
};

export const clearSelectOptions = selectKey => dispatch => {
  dispatch({
    type: CLEAR_SELECT_OPTIONS,
    selectKey
  })
};

export const loadClsObj = (clssArr, selectKey, dte = moment().format('YYYY-MM-DD')) => dispatch => {
    return Promise.all(clssArr.map(c => entity.getAllObjOfCls(c, dte)))
      .then(jsons => {
        if (jsons.every(json => json.success)) {
          const data = jsons.reduce((acc, json) => {
            return acc.concat(json.data);
          }, []);
          dispatch(addSelectOptions(selectKey, data));
          return data
        }
      })
};
export const loadClsObjWithProps = (clssArr, propConsts, dte = moment().format('YYYY-MM-DD')) => dispatch => {
    return Promise.all(clssArr.map(c => entity.getAllObjOfCls(c, dte, propConsts)))
      .then(jsons => {
        if (jsons.every(json => json.success)) {
          const data = jsons.map(json => json.data);
          return data
        }
      })
};
