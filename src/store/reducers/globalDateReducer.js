import { CHANGE_GLOBAL_DATE } from '../actions/globalDateActions';
import moment from "moment";

export default (state=moment(), action) => {
  switch (action.type) {
    case CHANGE_GLOBAL_DATE: return action.date;
    default: return state;
  }
}