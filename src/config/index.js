import axios from 'axios';
import forEach from 'lodash/forEach';

export const SYSTEM_LANG_ARRAY = ['kz', 'ru', 'en'];

export const mainInstance = axios.create({
  baseURL: `/${process.env.REACT_APP_APP_NAME}/a/archive/default`,
});

export const cubeRequests = {
  // getting cubeRequests itself
  getCube: (cubeConst, filters, nodeWithChilds, dte) => {
    const fd = new FormData();
    fd.append("cubeSConst", cubeConst);
    fd.append("filters", filters);
    fd.append("nodeWithChilds", nodeWithChilds);
    dte && fd.append("dte", dte);
    return mainInstance.post(`/${localStorage.getItem('i18nextLng')}/cube/getCubeData`, fd)
      .then(res => res.data);
  },
  createObj: (cube, obj) => {
    const fd = new FormData();
    fd.append("cube", JSON.stringify(cube));
    fd.append("obj", JSON.stringify(obj));
    return mainInstance.post(`/${localStorage.getItem('i18nextLng')}/cube/createObj`, fd)
      .then(res => res.data)
  },
  // updating values in cube
  updateCubeData: (cubeSConst, dte, datas, files) => {
    const fd = new FormData();
    fd.append("cubeSConst", cubeSConst);
    fd.append("dte", dte);
    fd.append("datas", datas);
    forEach(files, (fileArr, key) => {
      fileArr && fileArr.forEach((file, idx) => {
        file && !key.startsWith('__Q__') && fd.append(`files_${key}_${idx + 1}`, file);
        file && key.startsWith('__Q__') && fd.append(`filesQ_${key.split('__Q__')[1]}_${idx + 1}`, file);
      });
    });
    return mainInstance.post(`/${localStorage.getItem('i18nextLng')}/cube/saveCubeData`, fd)
      .then(res => res.data);
  },
};

// словари
export const handbookRequests = {
  // receiving all constants (first need)
  getAllConstants: () =>
    mainInstance.get(`/${localStorage.getItem('i18nextLng')}/utils/getAllConst`)
      .then(res => res.data),
  // getting access levels (first need)
  getAccessLevels: () =>
    mainInstance.get(`/${localStorage.getItem('i18nextLng')}/dict/getAccessLevel`)
      .then(res => res.data),
};

export const authRequests = {
  login: (login, password) => {
    const fd = new FormData();
    fd.append('login', login);
    fd.append('passwd', password);
    return mainInstance.post(`/${localStorage.getItem('i18nextLng')}/session/sessionIn`, fd)
      .then(res => res.data)
  },
  logout: () =>
    mainInstance.get(`/${localStorage.getItem('i18nextLng')}/Session/sessionOut`)
      .then(res => res.data),
  getUser: () =>
    mainInstance.get(`/${localStorage.getItem('i18nextLng')}/session/aboutMe`)
      .then(response => response.data),
  regNewUser: fd =>
    mainInstance.post(`/${localStorage.getItem('i18nextLng')}/registration/regUser`, fd),
  regNewUserWithECP: fd =>
    mainInstance.post(`/${localStorage.getItem('i18nextLng')}/registration/regUserWithECP`, fd),
  changePasswordAct: (paswd, userId) => {
    const fd = new FormData();
    fd.append('paswd', paswd);
    fd.append('obj', String(userId));
    return mainInstance.post(`/${localStorage.getItem('i18nextLng')}/Session/setPaswd`, fd)
      .then(res => res.data)
  }
};

export const entityRequest = {
  getPropValById: id =>
    mainInstance.get(`/${localStorage.getItem('i18nextLng')}/archive/getPropVal?propId=${id}`)
      .then(res => res.data),
  getPropValByConst: propConst =>
    mainInstance.get(`/${localStorage.getItem('i18nextLng')}/registration/getPropVal?propConst=${propConst}`)
      .then(res => res.data),
  getPropValWithChilds: propConst =>
    mainInstance.get(`/${localStorage.getItem('i18nextLng')}/registration/getPropValWithChilds?propConst=${propConst}`)
      .then(res => res.data),
  getObjChildsByConst: (c, props) =>
    mainInstance.get(`/${localStorage.getItem('i18nextLng')}/registration/getObjChilds?objConst=${c}&withProps=${props}`)
      .then(res => res.data),
  getObjChildsById: (id, props) =>
    mainInstance.get(`/${localStorage.getItem('i18nextLng')}/registration/getObjChilds?objId=${id}&withProps=${props}`)
      .then(res => res.data),
  getAllObjOfCls: (clsConst, dte, propConsts) =>
    mainInstance.get(`/${localStorage.getItem('i18nextLng')}/entity/getAllObjOfCls?clsConst=${clsConst}&dte=${dte}&propConsts=${propConsts}`)
      .then(res => res.data),
  // receiving blob of file by id.
  getFile: (id, type) =>
    mainInstance.get(`/${localStorage.getItem('i18nextLng')}/entity/getFile?idFile=${id}&type=${type}`, {responseType: 'blob'}),
  //delete file from db and cube
  dFile: (id, cubeSConst) =>
    mainInstance.get(`/${localStorage.getItem('i18nextLng')}/entity/dFile?idFile=${id}&cubeSConst=${cubeSConst}`)
      .then(res => res.data),
};