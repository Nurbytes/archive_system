import React from 'react';
import {Route, Switch} from "react-router-dom";
import Fundmakers from "./fundmakers";
import Asquisitions from "./asquisitions";
import Works from "./works";
import Reports from "./reports";

// Комплектование
const Manning = ({ t }) => {
    return (
      <Switch>
        <Route exact path="/manning/fundmakers" render={props => <Fundmakers t={t} {...props}/>} />
        <Route exact path="/manning/asquisitions" render={props => <Asquisitions t={t} {...props}/>} />
        <Route exact path="/manning/works" render={props => <Works t={t} {...props}/>} />
        <Route exact path="/manning/reports" render={props => <Reports t={t} {...props}/>} />
      </Switch>
    );
};

export default Manning;