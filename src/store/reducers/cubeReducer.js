import {
  GET_CUBE_PENDING,
  GET_CUBE_SUCCESS,
  GET_CUBE_FAIL,
  DESTROY_CUBE,
  CLEAR_FILTERS,
  CLEAR_ALL_CUBES
} from '../actions/cubeActions';

export default (state={}, action) => {
  const { cubeConst, type } = action;
  switch (type) {
    case GET_CUBE_PENDING: {
      return {
        ...state,
        [cubeConst]: {
          isFetching: true,
          error: null,
          filters: action.filters
        }
      }
    }
    case GET_CUBE_SUCCESS: {
      return {
        ...state,
        [cubeConst]: {
          filters: action.filters,
          data: action.data,
          error: null,
          isFetching: false
        }
      }
    }
    case GET_CUBE_FAIL: {
      return {
        ...state,
        [cubeConst]: {
          isFetching: false,
          error: action.error
        }
      }
    }
    case DESTROY_CUBE: {
      return {
        ...state,
        [action.key]: null
      }
    }
    case CLEAR_FILTERS: {
      return {
        ...state,
        [cubeConst]: {
          filters: null
        }
      }
    }
    case CLEAR_ALL_CUBES: {
      return {}
    }
    default: return state;
  }
}