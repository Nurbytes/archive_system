import React from 'react';
import {compose} from 'redux'
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import moment from 'moment';
import './header.scss';
// import BasketContent from './Basket/BasketContent'
// import {changeCaseInBasket, clearBasket, readingRoomMakeOrder} from '../actions/actions'
import gerb from '../../assets/img/Gerb.png';

// import {addCaseToBasket, logout, removeCaseFromBasket} from '../actions/actions';
import Menu from "antd/lib/menu";
import LoggedInNav from "./LoggedInNav";
import LoggedOutNav from "./LoggedOutNav";
import {withNamespaces} from "react-i18next";
import {logout} from "../../store/actions/authActions";
import {changeLng} from "../../store/actions/lngActions";

class Header extends React.Component{
  state = {
    showBasket: false,
    basketState: []
  };

  langMenu = (
    <Menu>
      <Menu.Item key="kz"><span onClick={() => this.changeLanguage('kz')}>kz</span></Menu.Item>
      <Menu.Item key="ru"><span onClick={() => this.changeLanguage('ru')}>ru</span></Menu.Item>
      <Menu.Item key="en"><span onClick={() => this.changeLanguage('en')}>en</span></Menu.Item>
    </Menu>
  );

  changeLanguage = (lng) => {
    // change lng in react-i18next
    this.props.i18n.changeLanguage(lng);
    // change lng in store
    this.props.changeLng(lng);
    switch (lng) {
      case 'en': moment.locale('en-gb'); break;
      case 'ru': moment.locale('ru'); break;
      case 'kz': moment.locale('kk'); break;
      default: moment.locale('kk'); break;
    }
  };

  showBasket = () => {
    this.setState({ showBasket: true, basketState: this.props.basket.slice() });
  };

  hideBasket = () => {
    // if(this.focusInCurrentTarget(e)) return;
    this.setState({showBasket: false});
  };

  render() {
    const { t, location, logout, removeCaseFromBasket, basket, addCaseToBasket, user, tofiConstants, changeCaseInBasket } = this.props;
    const { basketState, showBasket } = this.state;

    return (
      <header className="header">
        {this.props.children}
        <div className="header__logo" >
          <div className="header__logo__name">
            <h3>{t(`TITLE_1_1_${process.env.REACT_APP_APP_NAME}`)}</h3>
            <h3>{t(`TITLE_1_2_${process.env.REACT_APP_APP_NAME}`)}</h3>
          </div>
          <img src={gerb} alt="logo"/>
          <div className="header__logo__system">
            <h3>{t('TITLE_2_1')}</h3>
            <h3>{t('TITLE_2_2')}</h3>
          </div>
        </div>
        { user ?
          <LoggedInNav
            langMenu={this.langMenu}
            basket={basket}
            basketState={basketState}
            tofiConstants={tofiConstants}
            changeLanguage={this.changeLanguage}
            handleLogout={logout}
            t ={ t }
            showBasket={this.showBasket}
            hideBasket={this.hideBasket}
            basketIsShown={ showBasket }
            router={location}
            removeCaseFromBasket={removeCaseFromBasket}
            changeCaseInBasket={changeCaseInBasket}
            addCaseToBasket={addCaseToBasket}
            clearBasket={this.props.clearBasket}
            user={user}
          />
          :
          <LoggedOutNav
            langMenu={this.langMenu}
            changeLanguage={this.changeLanguage}
            t={ t }
          />
        }
      </header>
    )
  }
}

Header.propTypes = {
  logout: PropTypes.func.isRequired,
  t: PropTypes.func.isRequired,
  i18n: PropTypes.shape({
    changeLanguage: PropTypes.func.isRequired
  }).isRequired,
  basket: PropTypes.array,
  user: PropTypes.object
};

function mapStateToProps(state) {
  return {
    user: state.auth.user,
    basket: state.readingRoom.basket,
  }
}

export default compose(
  // connect(mapStateToProps, { logout, removeCaseFromBasket, addCaseToBasket, changeCaseInBasket, clearBasket } ),
  connect(mapStateToProps, { logout, changeLng }),
  withNamespaces('header')
)(Header);
