import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import lngReducer from "./reducers/lngReducer";
import cubeReducer from "./reducers/cubeReducer";
import handbookReducer from "./reducers/handbookReducer";
import authReducer from "./reducers/authReducer";
import tableReducer from "./reducers/tableReducer";
import selectReducer from "./reducers/selectReducer";
import readingRoomReducer from "./reducers/readingRoomReducer";
import globalDateReducer from './reducers/globalDateReducer';

const reducers = combineReducers({
    auth: authReducer,
    lng: lngReducer,
    handbooks: handbookReducer,
    table: tableReducer,
    select: selectReducer,
    readingRoom: readingRoomReducer,
    globalDate: globalDateReducer,
    // archiveFundPage,
    cubes: cubeReducer
});

export const store = createStore(reducers, {}, composeWithDevTools(applyMiddleware(thunk)));